export { default as Account } from "./Modulos/CadastrosGerais/Account";
export { default as Dashboard } from "./Modulos/Global/Dashboard";
export { default as NotFound } from "./Modulos/Global/NotFound";
export { default as ProductList } from "./ProductList";
export { default as Settings } from "./Modulos/Global/Settings";
export { default as SignIn } from "./Modulos/Global/SignIn";
export { default as SignUp } from "./Modulos/Global/SignUp";

export { default as UserList } from "./Modulos/CadastrosGerais/UserList";
export { default as ClienteInativos } from "./Modulos/CadastrosGerais/Pessoas/ClienteInativos";
export { default as ClienteAtivos } from "./Modulos/CadastrosGerais/Pessoas/ClienteAtivos";

export { defaultHome } from "./Modulos/Comercial";
export { ClientesAtivosList } from "./Modulos/Comercial/ClientesAtivos";
export { ClientesInativosList } from "./Modulos/Comercial/ClientesInativos";
export { Mapa } from "./Modulos/Comercial/Mapa"

