import React, { useState, useEffect } from "react";
import ReactMapGL, { Marker, Popup } from 'react-map-gl'
import axios from 'axios'
import PropTypes from 'prop-types';

import {
    makeStyles,
    Tabs,
    Tab,
    AppBar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Grid,
    Card,
    CardHeader,
    CardContent,
    Avatar,
    IconButton,
    Typography,
    TextField,
    Box,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    FormLabel
}
    from '@material-ui/core';


import {
    LocationOn as LocationOnIcon,
    HighlightOff as CloseIcon,
    AccountBox as AccountBoxIcon,
    ContactMail as ContactMailIcon,
    Description as DescriptionIcon,
    ShoppingBasket as ShoppingBasketIcon
} from '@material-ui/icons'

const { BASEURL } = require('../../../../common/server')

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(3),
    },
    rootCard: {
        maxWidth: 'auto',
        height: 'auto',
        minHeight: '500px'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    buttonImage: {
        background: 'none',
        border: 'none',
        cursor: 'pointer',
        width: '20px',
        height: '20px',
    },
    IconButton: {
        color: '#00FF00'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: '#000',
        height: '80px',
        width: "80px"
    },
    rootPanel: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

const Mapa = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [listClientes, setListClientes] = useState([])
    const [listaClientesFiltro, setListClientesFiltro] = useState([])

    const [listContatos, setListContatos] = useState([])
    const [listItens, setListItens] = useState([])
    const [listDocumentos, setListDocumentos] = useState([])
    const [selectCliente, setSelectCliente] = useState(null)

    const handleExpandClick = () => {
        setSelectCliente(null)
    };

    const handleChange = (event, newValue) => {
        event.preventDefault()
        setValue(newValue);
    };

    const [viewport, setViewport] = useState({
        latitude: -26.875598,
        longitude: -49.109647,
        width: '100%',
        height: 800,
        zoom: 7
    })


    const carregando = async () => {
        await axios
            .get(`${BASEURL}/pessoasMapa`)
            .then((resp) => {
                setListClientes(resp.data);
                setListClientesFiltro(resp.data)
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const contatos = async (id) => {
        const res = await axios
            .get(`${BASEURL}/pessoaContatos/${id}`)
        setListContatos(res.data)
    };

    const itens = async (id) => {
        const res = await axios
            .get(`${BASEURL}/produtosCliente/${id}`)
        setListItens(res.data)
    };

    const documentos = async (id) => {
        const res = await axios
            .get(`${BASEURL}/documentoItens/${id}`)
        setListDocumentos(res.data)
    };

    const filtro = (tipo) => {
        switch (tipo) {
            case 'T':
                return setListClientesFiltro(listClientes)
            case 'EHCONCRETEIRA':
                return setListClientesFiltro(listClientes.filter((e) => e.EHCONCRETEIRA === 'S'))
            case 'EHLABORATORIO':
                return setListClientesFiltro(listClientes.filter((e) => e.EHLABORATORIO === 'S'))
            case 'EHCONSTRUTORA':
                return setListClientesFiltro(listClientes.filter((e) => e.EHCONSTRUTORA === 'S'))
            case 'EHARGAMASSA':
                return setListClientesFiltro(listClientes.filter((e) => e.EHARGAMASSA === 'S'))
            default:
                return setListClientesFiltro(listClientes)
        }
    }

    useEffect(() => {
        carregando();
    }, []);

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid item lg={12} sm={12} xl={12} xs={12}>
                    <Card>
                        <CardContent>
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Filtro de Cliente</FormLabel>
                                <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                    <FormControlLabel
                                        value="EHTODOS"
                                        control={<Radio color="primary" />}
                                        label="Todos"
                                        labelPlacement="bottom"
                                        onClick={() => filtro('T')}
                                    />
                                    <FormControlLabel
                                        value="EHCONCRETEIRA"
                                        control={<Radio color="primary" />}
                                        label="Concreteiras"
                                        labelPlacement="bottom"
                                        onClick={() => filtro('EHCONCRETEIRA')}
                                    />
                                    <FormControlLabel
                                        value="EHLABORARIO"
                                        control={<Radio color="primary" />}
                                        label="Laboratório"
                                        labelPlacement="bottom"
                                        onClick={() => filtro('EHLABORATORIO')}
                                    />
                                    <FormControlLabel
                                        value="EHCONSTRUTORAS"
                                        control={<Radio color="primary" />}
                                        label="Construtoras"
                                        labelPlacement="bottom"
                                        onClick={() => filtro('EHCONSTRUTORA')}
                                    />
                                    <FormControlLabel
                                        value="EHARGAMASSA"
                                        control={<Radio color="primary" />}
                                        label="Argamassa"
                                        labelPlacement="bottom"
                                        onClick={() => filtro('EHARGAMASSA')}
                                    />

                                </RadioGroup>
                            </FormControl>
                            <ReactMapGL
                                {...viewport}
                                mapboxApiAccessToken="pk.eyJ1IjoiY3Jpc3RpYW5vdGFtYW5pbmkiLCJhIjoiY2tjY3NnMmhsMDdvYjJycHEwcXE5bms3dCJ9.xZJ7tfki2KdLIsoLHzRIpg"
                                onViewportChange={nextViewport => setViewport(nextViewport)}
                                mapStyle="mapbox://styles/cristianotamanini/ckccvftno1nk81jlv0vbizwnf">
                                {listaClientesFiltro.map((cli) => (
                                    <Marker key={cli.ID} latitude={cli.LATITUDE} longitude={cli.LONGITUDE}>
                                        <button className={classes.buttonImage} onClick={e => {
                                            e.preventDefault();
                                            setSelectCliente(cli)
                                            contatos(cli.ID)
                                            itens(cli.ID)
                                            documentos(cli.ID)
                                        }}>
                                             <LocationOnIcon style={{color: cli.STATUS}}/>
                                        </button>
                                    </Marker>
                                ))}
                                {selectCliente ? (
                                    <Popup
                                        latitude={selectCliente.LATITUDE}
                                        longitude={selectCliente.LONGITUDE}
                                        closeButton={false}
                                        closeOnClick={false}
                                        anchor="top"
                                    >
                                        <Card className={classes.rootCard}>
                                            <CardHeader
                                                avatar={
                                                    <Avatar
                                                        aria-label="recipe"
                                                        className={classes.avatar}
                                                        src={`${BASEURL}/imagePessoas/${selectCliente.CODIGO}.png`}
                                                    >
                                                        STU
                                    </Avatar>
                                                }
                                                action={
                                                    <IconButton onClick={handleExpandClick} aria-label="settings">
                                                        <CloseIcon />
                                                    </IconButton>
                                                }
                                                title={selectCliente.NOME}
                                                subheader={selectCliente.CODIGO}
                                            />
                                            <div className={classes.rootPanel}>
                                                <AppBar position="static" color="default">
                                                    <Tabs
                                                        value={value}
                                                        onChange={handleChange}
                                                        variant="scrollable"
                                                        scrollButtons="on"
                                                        indicatorColor="primary"
                                                        textColor="primary"
                                                        aria-label="scrollable force tabs example"
                                                    >
                                                        <Tab label="Geral" icon={<AccountBoxIcon />} {...a11yProps(0)} />
                                                        <Tab label="Contatos" icon={<ContactMailIcon />} {...a11yProps(1)} />
                                                        <Tab label="Itens" icon={<ShoppingBasketIcon />} {...a11yProps(2)} />
                                                        <Tab label="Documentos" icon={<DescriptionIcon />} {...a11yProps(3)} />
                                                    </Tabs>
                                                </AppBar>
                                                <TabPanel value={value} index={0}>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={2} sm={2} xl={2} xs={2}>
                                                            <TextField fullWidth id="inputCODIGO" label="Código" defaultValue={selectCliente.CODIGO} />
                                                        </Grid>
                                                        <Grid item lg={10} sm={10} xl={10} xs={10}>
                                                            <TextField fullWidth id="inputNOME" label="Nome" defaultValue={selectCliente.NOME} />
                                                        </Grid>
                                                    </Grid>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={2} sm={2} xl={2} xs={2}>
                                                            <TextField fullWidth id="inputCEP" label="CEP" defaultValue={selectCliente.CEP} />
                                                        </Grid>
                                                        <Grid item lg={2} sm={2} xl={2} xs={2}>
                                                            <TextField fullWidth id="inputTIPOLOGRADOURO" label="Tipo Logradouro" defaultValue={selectCliente.TIPOLOGR} />
                                                        </Grid>
                                                        <Grid item lg={6} sm={6} xl={6} xs={6}>
                                                            <TextField fullWidth id="inputLOGRADOURO" label="Logradouro" defaultValue={selectCliente.LOGRADOURO} />
                                                        </Grid>
                                                        <Grid item lg={2} sm={2} xl={2} xs={2}>
                                                            <TextField fullWidth id="inputNUMERO" label="Número" defaultValue={selectCliente.NUMERO} />
                                                        </Grid>
                                                    </Grid>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={3} sm={3} xl={3} xs={3}>
                                                            <TextField fullWidth id="inputCOMPLEMENTO" label="Complemento" defaultValue={selectCliente.COMPLEMENTO} />
                                                        </Grid>
                                                        <Grid item lg={3} sm={3} xl={3} xs={3}>
                                                            <TextField fullWidth id="inputBAIRRO" label="Bairro" defaultValue={selectCliente.BAIRRO} />
                                                        </Grid>
                                                        <Grid item lg={1} sm={1} xl={1} xs={1}>
                                                            <TextField fullWidth id="inputESTADO" label="Estado" defaultValue={selectCliente.ESTADO} />
                                                        </Grid>
                                                        <Grid item lg={5} sm={5} xl={5} xs={5}>
                                                            <TextField fullWidth id="inputMUNICIPIO" label="Múnicipio" defaultValue={selectCliente.MUNICIPIO} />
                                                        </Grid>
                                                    </Grid>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={3} sm={3} xl={3} xs={3}>
                                                            <TextField fullWidth id="inputCNPJCPF" label="CNPJ/CPF" defaultValue={selectCliente.CNPJCPF} />
                                                        </Grid>
                                                    </Grid>
                                                </TabPanel>
                                                <TabPanel value={value} index={1}>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={12} sm={12} xl={12} xs={12}>
                                                            <TableContainer component={Paper}>
                                                                <Table className={classes.table} aria-label="simple table">
                                                                    <TableHead>
                                                                        <TableRow>
                                                                            <TableCell align="left">Contato</TableCell>
                                                                            <TableCell align="left">DDD</TableCell>
                                                                            <TableCell align="left">Telefone</TableCell>
                                                                            <TableCell align="left">E-mail</TableCell>
                                                                        </TableRow>
                                                                    </TableHead>
                                                                    <TableBody>
                                                                        {listContatos.map((row) => (
                                                                            <TableRow key={row.ID}>
                                                                                <TableCell align="left">{row.CONTATO}</TableCell>
                                                                                <TableCell align="left">{row.DDD}</TableCell>
                                                                                <TableCell align="left">{row.TELEFONE}</TableCell>
                                                                                <TableCell align="left">{row.EMAIL}</TableCell>
                                                                            </TableRow>
                                                                        ))}
                                                                    </TableBody>
                                                                </Table>
                                                            </TableContainer>
                                                        </Grid>
                                                    </Grid>
                                                </TabPanel>
                                                <TabPanel value={value} index={2}>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={12} sm={12} xl={12} xs={12}>
                                                            <TableContainer component={Paper}>
                                                                <Table className={classes.table} aria-label="simple table">
                                                                    <TableHead>
                                                                        <TableRow>
                                                                            <TableCell align="left">Código</TableCell>
                                                                            <TableCell align="left">Descrição</TableCell>
                                                                            <TableCell align="left">Disponivel</TableCell>
                                                                        </TableRow>
                                                                    </TableHead>
                                                                    <TableBody>
                                                                        {listItens.map((row) => (
                                                                            <TableRow key={row.ID}>
                                                                                <TableCell align="left">{row.CODIGOPRODUTO}</TableCell>
                                                                                <TableCell align="left">{row.DESCRICAOPRODUTO}</TableCell>
                                                                                <TableCell align="left">{row.DISPONIVEL}</TableCell>
                                                                            </TableRow>
                                                                        ))}
                                                                    </TableBody>
                                                                </Table>
                                                            </TableContainer>
                                                        </Grid>
                                                    </Grid>
                                                </TabPanel>
                                                <TabPanel value={value} index={3}>
                                                    <Grid container spacing={1}>
                                                        <Grid item lg={12} sm={12} xl={12} xs={12}>
                                                            <TableContainer component={Paper}>
                                                                <Table className={classes.table} aria-label="simple table">
                                                                    <TableHead>
                                                                        <TableRow>
                                                                            <TableCell align="left">Documento</TableCell>
                                                                            <TableCell align="left">Data Emissão</TableCell>
                                                                            <TableCell align="left">Código</TableCell>
                                                                            <TableCell align="left">Descrição</TableCell>
                                                                            <TableCell align="left">Quantidade</TableCell>
                                                                            <TableCell align="left">R$ Unit.</TableCell>
                                                                            <TableCell align="left">Tipo Documento</TableCell>
                                                                        </TableRow>
                                                                    </TableHead>
                                                                    <TableBody>
                                                                        {listDocumentos.map((row) => (
                                                                            <TableRow key={row.ID}>
                                                                                <TableCell align="left">{row.DOCUMENTO}</TableCell>
                                                                                <TableCell align="left">{row.DATAEMISSAO}</TableCell>
                                                                                <TableCell align="left">{row.CODIGOSTUHLERT}</TableCell>
                                                                                <TableCell align="left">{row.PRODUTO}</TableCell>
                                                                                <TableCell align="left">{row.QUANTIDADE}</TableCell>
                                                                                <TableCell align="left">{row.VALORUNITARIO}</TableCell>
                                                                                <TableCell align="left">{row.TIPODOCUMENTO}</TableCell>
                                                                            </TableRow>
                                                                        ))}
                                                                    </TableBody>
                                                                </Table>
                                                            </TableContainer>
                                                        </Grid>
                                                    </Grid>
                                                </TabPanel>
                                            </div>
                                        </Card>
                                    </Popup>
                                ) : null}
                            </ReactMapGL>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
};

export default Mapa;