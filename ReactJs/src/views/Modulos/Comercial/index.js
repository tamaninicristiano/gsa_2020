export { default as defaultHome } from "./Home/HomeComercial";
export { default as defaultAtivosList } from "./ClientesAtivos/ClientesAtivosList";
export { default as defaultInativosList } from "./ClientesInativos/ClientesInativosList";
export { default as defaultMapa } from "./Mapa/Mapa"