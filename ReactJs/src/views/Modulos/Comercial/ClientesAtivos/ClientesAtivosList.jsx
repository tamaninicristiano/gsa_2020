import React from "react";
import { makeStyles } from "@material-ui/styles";

import { ClientesAtivosTable, ClientesAtivosToolbar } from "./components/index";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const ClientesAtivosList = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.divToolbar}>
        <ClientesAtivosToolbar />
      </div>

      <div className={classes.content}>
        <ClientesAtivosTable />
      </div>
    </div>
  );
};

export default ClientesAtivosList;
