import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import axios from "axios";

import ClientesInativosTable from "./components/ClientesInativosTable";
import ClientesInativosToolbar from "./components/ClientesInativosToolbar";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const ClientesInativosList = () => {
  const [clienteInativos, setListaCliInativos] = useState([]);

  const carregando = async (tipo) => {
    await axios
      .get(`http://192.168.0.241:3000/${tipo}`)
      .then((resp) => {
        setListaCliInativos(resp.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    carregando("clientesInativos");
  }, []);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.divToolbar}>
        <ClientesInativosToolbar />
      </div>

      <div className={classes.content}>
        <ClientesInativosTable lista={clienteInativos} />
      </div>
    </div>
  );
};

export default ClientesInativosList;
