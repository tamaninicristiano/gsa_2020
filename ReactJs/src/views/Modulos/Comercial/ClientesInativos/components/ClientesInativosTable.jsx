import React from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import PerfectScrollbar from "react-perfect-scrollbar";
import { makeStyles } from "@material-ui/styles";
import {
  Card,
  CardContent,
  Avatar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";

import { getInitials } from "helpers";

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
  nameContainer: {
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    marginRight: theme.spacing(2),
    width: theme.spacing(6),
    height: theme.spacing(6),
    justifyContent: "center",
  },
  actions: {
    justifyContent: "flex-end",
  },
}));

const ClientesInativosTable = (props) => {
  const { className, lista, ...rest } = props;

  const classes = useStyles();

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell>Código</TableCell>
                  <TableCell>Descrição</TableCell>
                  <TableCell>CNPJ/CPF</TableCell>
                  <TableCell>UF</TableCell>
                  <TableCell>Município</TableCell>
                  <TableCell>Última Venda</TableCell>
                  <TableCell>Sit. Financeiro</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {lista.map((pessoa) => {
                  return (
                    <TableRow component={Link} to={`/ClienteInativo/${pessoa.ID}`} params={{ ID: pessoa.ID }} key={pessoa.ID} >
                      <TableCell>
                        <div className={classes.nameContainer}>
                          <Avatar
                            className={classes.avatar}
                            src={`http://192.168.0.241:3000/imagePessoas/${pessoa.CODIGO}.png`}
                          >
                            {getInitials(pessoa.PESSOA)}
                          </Avatar>
                        </div>
                      </TableCell>
                      <TableCell>{pessoa.CODIGO}</TableCell>
                      <TableCell>{pessoa.PESSOA}</TableCell>
                      <TableCell>{pessoa.CNPJCPF}</TableCell>
                      <TableCell>{pessoa.UF}</TableCell>
                      <TableCell>{pessoa.MUNICIPIO}</TableCell>
                      <TableCell>{pessoa.ULTIMAVENDA}</TableCell>
                      <TableCell style={{color:`${pessoa.COR}`, fontWeight: 'bold'}}>{pessoa.SITUACAOFINANCEIRO}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>
  );
};

ClientesInativosTable.propTypes = {
  className: PropTypes.string,
};

export default ClientesInativosTable;
