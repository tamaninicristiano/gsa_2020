import React, { useState, useEffect} from "react";
import { makeStyles } from "@material-ui/styles";
import { Link as RouterLink } from "react-router-dom";
import { Grid } from "@material-ui/core";
import axios from "axios";

import {
  CardClientesAtivos,
  CardClientesInativos,
  CotasProgresso,
  CardFaturamento,
  LatestSales,
  FaturamentoMensal
} from "./components";

const { BASEURL } = require('../../../../common/server')

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}));

const HomeComercial = () => {
  const classes = useStyles();

  const [state, setState] = useState({
    qtdAtivo: 0,
    qtdInativo: 0,
    qtdProgCotas: 0,
    valorFaturamento: "R$ 0,00",
  });

  const [labelsGrafico] = useState([1,2,3,4,8,9,10,11,14,15,16,17,18,21,22,23,24,25,28,29,30]);
  const [dataMeta] = useState([9763,14778,19689,27635,42344,53490,59215,64822,73892,90685,107294,115827,124181,137698,162723,178335,186355,194208,206913,230435,250000]);
  const [dataReal, setDataReal] = useState([])

  const [labelsGraficoMensal] = useState(['01','02','03','04','05','06','07','08','09','10','11','12',]);
  const [dataAnoPassado] = useState([125728,148379,192191,188498,157882,149469,163501,158084,218322,254022,202651,113975]);
  const [dataAnoAtual, setDataAnoAtual] = useState([])

  const carregando = async () => {
    await axios
      .get(`${BASEURL}/valorFaturamentoAcumulado`)
      .then((resp) => {
        const faturamento = resp.data
        const result = faturamento.map(a => a.VALORACUMULADO)
        setDataReal(result)
      })
      .catch((err) => {
        console.log(err);
      });

    await axios
      .get(`${BASEURL}/clientesAtivos`)
      .then((resp) => {
        setState((state) => ({ ...state, qtdAtivo: resp.data.length }));
      })
      .catch((err) => {
        console.log(err);
      });

    await axios
      .get(`${BASEURL}/clientesInativos`)
      .then((resp) => {
        setState((state) => ({ ...state, qtdInativo: resp.data.length }));
      })
      .catch((err) => {
        console.log(err);
      });

    await axios
      .get(`${BASEURL}/valorFaturamento`)
      .then((resp) => {
        if (resp.data[0].VALORLIQUIDO) {
          setState((state) => ({
            ...state,
            valorFaturamento: resp.data[0].VALORLIQUIDO,
          }));
        }
      })
      .catch((err) => {
        console.log(err);
      });

      await axios
      .get(`${BASEURL}/Faturamento2020`)
      .then((resp) => {
        const faturamento = resp.data
        const result = faturamento.map(a => a.VALORLIQUIDO)
        setDataAnoAtual(result)
      })
      .catch((err) => {
        console.log(err);
      });


    await axios
      .get(`${BASEURL}/cotaGeral`)
      .then((resp) => {
        setState((state) => ({
          ...state,
          qtdProgCotas: resp.data[0].COTAGERAL,
        }));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    carregando();
    setInterval( function() {
      carregando()
    }, 60000 );
  }, []);

  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <RouterLink to="/ListClientesAtivos">
            <CardClientesAtivos qtdativos={state.qtdAtivo} percentual="12" />
          </RouterLink>
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <RouterLink to="/ListClientesInativos">
            <CardClientesInativos
              qtdinativo={state.qtdInativo}
              percentual="88"
            />
          </RouterLink>
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <CotasProgresso percentualprog={state.qtdProgCotas} />
        </Grid>
        <Grid item lg={3} sm={6} xl={3} xs={12}>
          <CardFaturamento vlrfaturamento={state.valorFaturamento} />
        </Grid>
        <Grid item lg={6} md={6} xl={6} xs={12}>
          <LatestSales dmeta={dataMeta} dreal={dataReal} rotulosgrafico={labelsGrafico} />
        </Grid>
        <Grid item lg={6} md={6} xl={6} xs={12}>
          <FaturamentoMensal anopassado={dataAnoPassado} anoatual={dataAnoAtual} rotulosgrafico={labelsGraficoMensal} />
        </Grid>
      </Grid>
    </div>
  );
};

export default HomeComercial;