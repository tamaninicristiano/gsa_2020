import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar } from '@material-ui/core';
import {
  ArrowDownward as ArrowDownwardIcon,
  PersonAddDisabled as PersonAddDisabledIcon
} from '@material-ui/icons/';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700
  },
  avatar: {
    backgroundColor: theme.palette.error.dark,
    height: 56,
    width: 56
  },
  icon: {
    height: 32,
    width: 32
  },
  difference: {
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center'
  },
  differenceIcon: {
    color: theme.palette.error.dark
  },
  differenceValue: {
    color: theme.palette.error.dark,
    marginRight: theme.spacing(1)
  }
}));

const CardClientesInativos = (props) => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <Grid container justify="space-between">
          <Grid item>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
              variant="body2">
              CLIENTES INATIVOS
            </Typography>
            <Typography variant="h3">{props.qtdinativo}</Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <PersonAddDisabledIcon className={classes.icon} />
            </Avatar>
          </Grid>
        </Grid>
        <div className={classes.difference}>
          <ArrowDownwardIcon className={classes.differenceIcon} />
          <Typography className={classes.differenceValue} variant="body2">
            {props.percentual} %
          </Typography>
          <Typography className={classes.caption} variant="caption">
            de representação
          </Typography>
        </div>
      </CardContent>
    </Card>
  );
};

CardClientesInativos.propTypes = {
  className: PropTypes.string
};

export default CardClientesInativos;
