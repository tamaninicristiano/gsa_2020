export { default as PessoasList } from "./PessoasList";
export { default as ClienteInativos } from "./ClienteInativos";
export { default as ClienteAtivos } from "./ClienteAtivos";
