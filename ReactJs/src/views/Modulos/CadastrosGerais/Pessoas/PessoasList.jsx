import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";

import { PessoasTable, PessoasToolbar } from "./components";
import mockData from "./data";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const PessoasList = () => {
  const classes = useStyles();

  const [clientesAtivos] = useState(mockData);

  return (
    <div className={classes.root}>
      <div className={classes.divToolbar}>
        <PessoasToolbar />
      </div>

      <div className={classes.content}>
        <PessoasTable clientes={clientesAtivos} />
      </div>
    </div>
  );
};

export default PessoasList;
