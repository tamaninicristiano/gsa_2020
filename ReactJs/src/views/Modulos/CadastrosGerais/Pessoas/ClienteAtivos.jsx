import React, {useState, useEffect } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

import {
    makeStyles,
    Tabs,
    Tab,
    AppBar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Grid,
    Card,
    CardContent,
    Typography,
    TextField,
    Box,
    IconButton 
}
    from '@material-ui/core';

import {
    AccountBox as AccountBoxIcon,
    ContactMail as ContactMailIcon,
    Description as DescriptionIcon,
    ShoppingBasket as ShoppingBasketIcon
} from '@material-ui/icons'

const { BASEURL } = require('../../../../common/server')

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(1),
    },
    rootCard: {
        display: 'flex',
        height: 'auto',
        padding: theme.spacing(1),
    },
    rootCardHead: {
        padding: theme.spacing(1),
        marginBottom: '10px'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    buttonImage: {
        background: 'none',
        border: 'none',
        cursor: 'pointer',
        width: '20px',
        height: '20px',
    },
    IconButton: {
        color: '#00FF00'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: '#000',
        height: '80px',
        width: "80px"
    },
    rootPanel: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography component={'span'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


function ClienteInativos(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [listContatos, setListContatos] = React.useState([])
    const [listItens, setListItens] = React.useState([])
    const [listDocumentos, setListDocumentos] = React.useState([])
    const [dadoCliente, setDadoCliente] = useState({
        CODIGO: 0,
        NOME: '',
        CEP: '',
        TIPOLOGRADOURO: '',
        LOGRADOURO: '',
        NUMERO: '',
        BAIRRO:'',
        COMPLEMENTO: '',
        UF: '',
        MUNICIPIO: '',
        CNPJCPF: ''
    })

    const handleChange = (event, newValue) => {
        event.preventDefault()
        setValue(newValue);
    };

    function a11yProps(index) {
        return {
            id: `scrollable-force-tab-${index}`,
            'aria-controls': `scrollable-force-tabpanel-${index}`,
        };
    }

    function HandleChange(event) {
        setDadoCliente({value: event.target.value});
      }

   const carregando = async () => {    
            await axios
            .get(`${BASEURL}/pessoa/${props.match.params.ID}`)
            .then((resp) => {
                setDadoCliente(resp.data[0]);
            })
            .catch((err) => {
                console.log(err);
            });

            await axios
            .get(`${BASEURL}/pessoaContatos/${props.match.params.ID}`)
            .then((resp) => {
                setListContatos(resp.data)
            })
            .catch((err) => {
                console.log(err);
            });

            await axios
            .get(`${BASEURL}/produtosCliente/${props.match.params.ID}`)
            .then((resp) => {
                setListItens(resp.data)
            })
            .catch((err) => {
                console.log(err);
            });
         
            await axios
            .get(`${BASEURL}/documentoItens/${props.match.params.ID}`)
            .then((resp) => {
                setListDocumentos(resp.data)
            })
            .catch((err) => {
                console.log(err);
            });
    }

    useEffect(() => { 
        carregando();
        // eslint-disable-next-line
    }, []);


    return (    
        <div className={classes.root}>
             <Card className={classes.rootCardHead} >
                 <CardContent>
                 <div style={{ display: 'flex', flexDirection: 'row',  justifyContent:'space-between'}}>
                        <div>
                            <Typography variant="h3">
                                <AccountBoxIcon /> Informações do Cliente 
                            </Typography>
                        </div>
                        <div>
                            <Link to="/ListClientesAtivos">
                                <IconButton color="primary" aria-label="add an alarm">
                                    <MeetingRoomIcon />
                                </IconButton>
                            </Link>
                        </div>
                    </div>
                 </CardContent>
             </Card>
             <Grid container spacing={1}>
                <Grid item lg={12} sm={12} xl={12} xs={12}>
                    <Card>
                    <CardContent>
                    <div className={classes.rootPanel}>
                            <AppBar position="static" color="default">
                                <Tabs
                                    value={value}
                                    onChange={handleChange}
                                    variant="scrollable"
                                    scrollButtons="on"
                                    indicatorColor="primary"
                                    textColor="primary"
                                    aria-label="scrollable force tabs example"
                                >
                                    <Tab label="Geral" icon={<AccountBoxIcon />} {...a11yProps(0)} />
                                    <Tab label="Contatos" icon={<ContactMailIcon />} {...a11yProps(1)} />
                                    <Tab label="Itens" icon={<ShoppingBasketIcon />} {...a11yProps(2)} />
                                    <Tab label="Documentos" icon={<DescriptionIcon />} {...a11yProps(3)} />
                                </Tabs>
                            </AppBar>
                            <TabPanel value={value} index={0}>
                                <Grid container spacing={1}>
                                    <Grid item lg={2} sm={2} xl={2} xs={2}>
                                    <TextField fullWidth id="inputCODIGO" label="Código"  value={dadoCliente.CODIGO} onChange={HandleChange}/> 
                                    </Grid>
                                    <Grid item lg={10} sm={10} xl={10} xs={10}>
                                        <TextField fullWidth id="inputNOME" label="Nome"  value={dadoCliente.NOME} onChange={HandleChange}/>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={1}>
                                    <Grid item lg={2} sm={2} xl={2} xs={6}>
                                        <TextField fullWidth id="inputCEP" label="CEP"  value={dadoCliente.CEP} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={2} sm={2} xl={2} xs={6}>
                                        <TextField fullWidth id="inputTIPOLOGRADOURO" label="Tipo Log."  value={dadoCliente.TIPOLOGRADOURO} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={6} sm={8} xl={6} xs={12}>
                                        <TextField fullWidth id="inputLOGRADOURO" label="Logradouro" value={dadoCliente.LOGRADOURO} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={2} sm={12} xl={2} xs={6}>
                                        <TextField fullWidth id="inputNUMERO" label="Número" value={dadoCliente.NUMERO} onChange={HandleChange}/>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={1}>
                                    <Grid item lg={3} sm={4} xl={3} xs={12}>
                                        <TextField fullWidth id="inputCOMPLEMENTO" label="Complemento" value={dadoCliente.COMPLEMENTO} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={3} sm={8} xl={3} xs={12}>
                                        <TextField fullWidth id="inputBAIRRO" label="Bairro" value={dadoCliente.BAIRRO} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={3} sm={3} xl={1} xs={4}>
                                        <TextField fullWidth id="inputESTADO" label="Estado" value={dadoCliente.UF} onChange={HandleChange}/>
                                    </Grid>
                                    <Grid item lg={3} sm={9} xl={5} xs={8}>
                                        <TextField fullWidth id="inputMUNICIPIO" label="Múnicipio" value={dadoCliente.MUNICIPIO} onChange={HandleChange}/>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={1}>
                                    <Grid item lg={3} sm={12} xl={3} xs={12}>
                                        <TextField fullWidth id="inputCNPJCPF" label="CNPJ/CPF" value={dadoCliente.CNPJCPF} onChange={HandleChange}/>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={1}>
                                <Grid container spacing={1}>
                                    <Grid item lg={12} sm={12} xl={12} xs={12}>
                                        <TableContainer component={Paper}>
                                            <Table className={classes.table} aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left">Contato</TableCell>
                                                        <TableCell align="left">DDD</TableCell>
                                                        <TableCell align="left">Telefone</TableCell>
                                                        <TableCell align="left">E-mail</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {listContatos.map((row) => (
                                                        <TableRow key={row.ID}>
                                                            <TableCell align="left">{row.CONTATO}</TableCell>
                                                            <TableCell align="left">{row.DDD}</TableCell>
                                                            <TableCell align="left">{row.TELEFONE}</TableCell>
                                                            <TableCell align="left">{row.EMAIL}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={2}>
                                <Grid container spacing={1}>
                                    <Grid item lg={12} sm={12} xl={12} xs={12}>
                                        <TableContainer component={Paper}>
                                            <Table className={classes.table} aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left">Código</TableCell>
                                                        <TableCell align="left">Descrição</TableCell>
                                                        <TableCell align="left">Disponivel</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {listItens.map((row) => (
                                                        <TableRow key={row.ID}>
                                                            <TableCell align="left">{row.CODIGOPRODUTO}</TableCell>
                                                            <TableCell align="left">{row.DESCRICAOPRODUTO}</TableCell>
                                                            <TableCell align="left">{row.DISPONIVEL}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={3}>
                                <Grid container spacing={1}>
                                    <Grid item lg={12} sm={12} xl={12} xs={12}>
                                        <TableContainer component={Paper}>
                                            <Table className={classes.table} aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left">Documento</TableCell>
                                                        <TableCell align="left">Data Emissão</TableCell>
                                                        <TableCell align="left">Código</TableCell>
                                                        <TableCell align="left">Descrição</TableCell>
                                                        <TableCell align="left">Quantidade</TableCell>
                                                        <TableCell align="left">R$ Unit.</TableCell>
                                                        <TableCell align="left">Tipo Documento</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {listDocumentos.map((row) => (
                                                        <TableRow key={row.ID}>
                                                            <TableCell align="left">{row.DOCUMENTO}</TableCell>
                                                            <TableCell align="left">{row.DATAEMISSAO}</TableCell>
                                                            <TableCell align="left">{row.CODIGOSTUHLERT}</TableCell>
                                                            <TableCell align="left">{row.PRODUTO}</TableCell>
                                                            <TableCell align="left">{row.QUANTIDADE}</TableCell>
                                                            <TableCell align="left">{row.VALORUNITARIO}</TableCell>
                                                            <TableCell align="left">{row.TIPODOCUMENTO}</TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Grid>
                                </Grid>
                            </TabPanel>
                        </div>
                    </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </div>
    )
}

export default ClienteInativos
