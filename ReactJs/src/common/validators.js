const checked = (value, options) => {
  if (value !== true) {
    return options.message || 'deve ser verificado';
  }
};

export default {
  checked
};
