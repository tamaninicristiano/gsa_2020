import React from 'react'
import {
    Typography
}
    from '@material-ui/core';

function textLeitura(props) {
    

    return (
        <div style={{ marginLeft: '7px', justifyContent: 'center', borderBottomWidth: '1px', borderColor: '#000'}}>
            <div>
                <Typography style={{fontSize: '0.7rem'}}>{props.label}</Typography>
            </div>
            <div >
                <Typography style={{fontSize: '0.9rem'}}>{props.value}</Typography>
            </div>
        </div>
    )
}

export default textLeitura
