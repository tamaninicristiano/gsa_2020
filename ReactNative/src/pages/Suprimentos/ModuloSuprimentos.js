import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'
import Body from '../../components/Template/Body'

import ButtonCard from '../../components/Estrutura/ButtonCard'
import TitleScreen from '../../components/Estrutura/TitleScreen'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'

export default class ModuloSuprimentos extends Component {
    OpenPage = (tela) => {
        this.props.navigation.navigate(tela);
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name='bars' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title="Módulo Suprimentos" />
                    <View style={styles.Body}>
                        <View style={styles.containerItem}>
                            <TouchableOpacity onPress={() => this.OpenPage('Produtos')}>
                                <ButtonCard title="Produtos" icone="cubes" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaSolicitacaoCompraItens')}>
                                <ButtonCard title="Solicitações de Compra" icone="list-alt" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity >
                                <ButtonCard title="Ordens de Compra" icone="check-square-o" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity >
                                <ButtonCard title="Fornecedores" icone="users" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity >
                                <ButtonCard title="Entrada de Materiais" icone="shopping-cart" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ProdutoDetalhes')}>
                                <ButtonCard title="Produto QRCode" icone="qrcode" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaInventarios')}>
                                <ButtonCard title="Inventários" icone="archive" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}></Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    Body: {
        alignItems: 'center',
    },
    containerItem: {
        padding: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    cardBig: {
        backgroundColor: '#FFF',
    }
})