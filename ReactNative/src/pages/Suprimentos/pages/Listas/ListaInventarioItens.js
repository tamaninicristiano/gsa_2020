import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList, Image, TextInput, TouchableOpacity, Alert, Modal } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import axios from 'axios'

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'
import ImagBack from '../../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'
import InventarioItens from '../../components/InventarioItens'
import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import QRCode from '../../../Global/components/qrCode'
import ImageCard from '../../../../components/Estrutura/ImageCard'

import AsyncStorage from '@react-native-community/async-storage'

export default class ListaInventarioItens extends Component {
    state = {
        ip: "",
        porta: "",
        produtos: [],
        placeHolderCodigo: 'Código do Produto:',
        placeHolderQuantidade: 'Quantidade',
        inventarioItens: [],
        showModal: false,
        showQRCodeModal: false,
        CODIGOFILTRO: null,

        ITEM: null,
        CODIGO: 0,
        ITEM: null,
        PRODUTO: null,
        QUANTIDADE: 0,
        CUSTO: 0,
        CUSTOTOTAL: 0,
        DISPONIVEL: 0,
        DIFERENCA: 0
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregarInventarioItens()
    }

    carregarInventarioItens = async () => {
        try {
            const params = await this.props.navigation.state.params;
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/inventarioitens/${params.ID}`)
            await this.setState({ inventarioItens: res.data })
            await this.setState({ INVENTARIO: params.ID })  
        } catch (err) {
            Alert.alert('Atenção', 'Erro de conexão.')
        }
    }

    onPress = async () => {
        if (this.state.CODIGOFILTRO != '') {
            try {
               await this.setState({
                    CODIGO: this.state.CODIGOFILTRO
                })
                const resP = await axios.get(`${this.state.ip}:${this.state.porta}/produtoInventario/${this.state.CODIGO}`)
                if(resP.data[0].ID !== '') {
                    await this.setState({
                        ITEM: resP.data[0].ID,
                        CODIGO: resP.data[0].CODIGO,
                        PRODUTO: resP.data[0].PRODUTO,
                        CUSTO: resP.data[0].VALORCUSTO,
                        DISPONIVEL: resP.data[0].DISPONIVEL
                    })
                } else {
                    Alert.alert('Atenção', 'Item não encontrado!')
                }
            } catch (err) {
                console.log(err)
            }
        } else {
            Alert.alert('Atenção', 'Informe o código do produto.')
        }
    }

    onSave = async () => {
        try {
            const params = await this.props.navigation.state.params
            await axios.post(`${this.state.ip}:${this.state.porta}/inventarioItem/${params.ID}`, {
                ITEM: this.state.ITEM,
                QUANTIDADE: this.state.QUANTIDADE,
                CUSTO: this.state.CUSTO,
                CUSTOTOTAL: this.state.QUANTIDADE * this.state.CUSTO,
                DISPONIVEL: this.state.DISPONIVEL,
                DIFERENCA: this.state.QUANTIDADE - this.state.DISPONIVEL
            })

            await this.setState({showModal: false})
            await this.setState({showModal: true})
        } catch (err) {
            Alert.alert(`${err}`)
        }
    }

    FiltroProduto = async FiltroProduto => {
        await this.setState({ showQRCodeModal: false })
        await this.setState({ CODIGOFILTRO: FiltroProduto })
        this.setState({ placeHolderCodigo: `${FiltroProduto}` })
        await this.onPress()
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal style={styles.modal} visible={this.state.showModal}
                    animationType='fade'>
                    <View style={styles.containerFiltro}>
                        <QRCode isVisible={this.state.showQRCodeModal}
                            onCancel={() => this.setState({ showQRCodeModal: false })}
                            onFiltrar={this.FiltroProduto}
                        />   
                        <Header />
                        <Body img={ImagemBack}>
                        <TitleScreen title='Cadastro de Item' />
                        <View style={styles.containerForm}>
                            <View style={styles.input}>
                                <View style={styles.Icon}>
                                    <Icon name='archive' size={20} color='#000000' />
                                </View>
                                <View style={{ width: '80%', alignItems: 'center' }}>
                                    <TextInput keyboardType='numeric' placeholder={this.state.placeHolderCodigo}
                                       onChangeText={codigo => this.setState({ CODIGOFILTRO: codigo })}                
                                       onEndEditing={this.onPress} textAlign='center'
                                    />
                                </View>
                            </View>
                            <View style={styles.input}>
                                <View style={styles.Icon}>
                                    <Icon name='check-square-o' size={20} color='#000000' style={{ marginLeft: 15, width: 25 }} />
                                </View>
                                <View style={{ width: '80%', alignItems: "center" }}>
                                    <TextInput keyboardType='numeric' placeholder={this.state.placeHolderQuantidade} textAlign='center'
                                        onChangeText={quantidade => this.setState({ QUANTIDADE: quantidade })}
                                        name='txtQuantidade'
                                        clearTextOnFocus={this.state.clearText}
                                    />
                                </View>
                                <View style={{ width: '10%', }} />
                            </View>
                        </View>
                        <ImageCard style={styles.Card}>
                            <View style={styles.containerCard}>
                                <View style={styles.containerCardHeader}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                                        {this.state.CODIGO} -  {this.state.PRODUTO}
                                    </Text>
                                </View>
                                <View style={styles.containerCardBody}>
                                    <Image source={{ uri: `${this.state.ip}:${this.state.porta}/image/${this.state.CODIGO}.png` }} style={styles.foto} />
                                </View>
                            </View>
                        </ImageCard>
                            
                        </Body>
                        <Footer>
                        <ButtonsTreeFooter
                            onClickButtonLeft={() => {this.setState({showModal: false})}}
                            onClickButtonCenter={() => {this.setState({showQRCodeModal: true})}}
                            onClickButtonRight={() => this.onSave()}

                            IconLeft='close'
                            IconPrincipal='qrcode'
                            IconRight='save'

                            colorLeft='#8A0808'
                            colorPrincipal='#000000'
                            colorRight='#298A08'
                        />
                    </Footer>
                    </View>
                </Modal >
                <Header />
                <Body img={ImagBack}>
                    <FlatList data={this.state.inventarioItens}
                            keyExtractor={item => `${item.ID}`}
                            renderItem={({ item }) => 
                                <InventarioItens id={item.ID} 
                                            item={item.ITEM} 
                                            image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`}
                                            codigo={item.CODIGO}
                                            produto={item.PRODUTO}
                                            quantidade={item.QUANTIDADE}
                                            custo={item.CUSTO}
                                            custototal={item.CUSTOTOTAL}
                                            disponivel={item.DISPONIVEL}
                                            diferenca={item.DIFERENCA}
                                />
                            }/>
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonLeft={this.carregarInventarioItens}
                        onClickButtonCenter={() => {this.setState({showModal: true})}}
                        onClickButtonRight={this.OpenPageComponentesProduto}

                        IconLeft='refresh'
                        IconPrincipal='plus'
                        IconRight='shopping-cart'

                        colorLeft='#8A0808'
                        colorPrincipal='#000000'
                        colorRight='#298A08'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    modal: {
        flex: 1
    },
    containerFiltro: {
        flex: 1
    },
    containerForm: {
        justifyContent: "center",
        alignItems: 'center'
    },
    Icon: {
        width: '10%', justifyContent: 'center', alignItems: 'center'
    },
    input: {
        height: 50,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    inputTexto: {
        height: 300,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    Card: {
        marginLeft: 15,
        marginRight: 15,
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    foto: {
        height: 220,
        width: 220
    },
    InfAdicional: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 80
    },
    fotoPessoa: {
        height: 150,
        width: 150
    },
    imagePessoa: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        height: 55,
        width: 55
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    date: {
        fontSize: 20
    }

})
