import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, FlatList, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'

import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import SolicitacaoCompra from '../../components/SolicitacaoCompra'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'

export default class ListaSolicitacoesCompra extends Component {
    state = {
        ip: null,
        porta: null,

        solicitacoesCompra: [],
    }

    componentDidMount = async () => {
        await this.conexaoDB();
        await this.carregar();
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregar = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/solicitacaoCompraItens`)
            this.setState({
                solicitacoesCompra: res.data
            })
        } catch (err) {
            Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
        }
    }

    OpenPage = (tela, item) => {
        this.props.navigation.navigate(tela, item);
    };

    addItem = async () => {
        await this.OpenPage('SolicitacaoCompraItem')
    }

    EditarSolicitacao = (item) => {
        this.OpenPage('SolicitacaoCompraItem', item)
    }

    deleteSolicitacao = async (ID, SITUACAO) => {
        if (SITUACAO === 1) {
            try {
                await axios.delete(`${this.state.ip}:${this.state.porta}/removeSolicitacaoCompraItem/${ID}`)
                await this.carregar()
            } catch (err) {
                Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
            }
        } else {
            Alert.alert('Atenção', 'Somente é possível excluir na situação de cadastrado!')
        }

    }

    confirmaSolicitacao = async (ID) => {
        try {
            await axios.put(`${this.state.ip}:${this.state.porta}/updateSituacaoSolicitacaoItem/${ID}`)
            await this.setState({
                solicitacoesCompra: []
            })
            await this.carregar()
        } catch (err) {
            Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
        }
    }

    PerguntaDelete = async (msg, ID, SITUACAO) => {
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.deleteSolicitacao(ID, SITUACAO) },
            ],
            { cancelable: false },
        );
    }

    perguntaCancel = async () => {
        await this.setState({
            solicitacoesCompra: []
        })
        await this.carregar()
    }

    PerguntaConfirmar = async (msg, ID) => {
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Cancelar',
                    onPress: () => this.perguntaCancel(),
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.confirmaSolicitacao(ID) },
            ],
            { cancelable: false },
        );
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={'Solicitações de Compra'} />
                    <FlatList data={this.state.solicitacoesCompra}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => this.EditarSolicitacao(item)}>
                                <SolicitacaoCompra
                                    id={item.ID}
                                    situacao={item.SITUACAO}
                                    codigo={item.CODIGO}
                                    data={item.DATA}
                                    produto={item.PRODUTO}
                                    quantidade={item.QUANTIDADE}
                                    ordemCompra={item.ORDEMCOMPRA}
                                    situacaoCor={item.SITUACAOCOR}
                                    image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`}
                                    onDelete={this.PerguntaDelete}
                                    onConfirmar={this.PerguntaConfirmar}
                                />
                            </TouchableOpacity>}
                    />
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonLeft={this.carregar}
                        onClickButtonCenter={this.addItem}
                        onClickButtonRight={this.OpenPageComponentesProduto}

                        IconLeft='refresh'
                        IconPrincipal='plus'
                        IconRight='shopping-cart'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    Body: {
        alignItems: 'center',
    },

})
