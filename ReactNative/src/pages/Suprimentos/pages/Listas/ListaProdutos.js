import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text, FlatList } from 'react-native'
import Header from '../../../../components/Template/Header'
import Footer from '../../../../components/Template/Footer'
import Body from '../../../../components/Template/Body'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import ProdutoLista from '../../components/ProdutoLista'

export default class Menu extends Component {
    state = {
        ip: "",
        porta: "",
        produtos: []
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.loadProdutos()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    loadProdutos = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/produtos`)
            this.setState({ produtos: res.data })
        } catch (err) {
            showError(err)
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <FlatList data={this.state.produtos}
                        keyExtractor={item => `${item.CODIGO}`}
                        renderItem={({ item }) => <ProdutoLista codigo={item.CODIGO} produto={item.PRODUTO} prateleira={item.PRATELEIRA} image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`} />} />
                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}></Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },


})
