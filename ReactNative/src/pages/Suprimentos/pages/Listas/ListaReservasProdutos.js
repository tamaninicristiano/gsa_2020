import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, Alert, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'

import ReservaProduto from '../../components/ReservaProduto'

export default class ListaReservaProduto extends Component {
    state = {
        ip: '',
        porta: '',

        reservasProduto: []
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregamento()

    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregamento = async () => {
        try {
            const COD = await this.props.navigation.state.params;
            //await Alert.alert('Atenção', `${COD}`)
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/reservasProduto/${COD}`)
            await this.setState({
                reservasProduto: res.data
            })
        } catch (err) {
            Alert.alert('Atenção', 'Não foram encontradas reservas para o item selecionado.')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={'Listagem de Reservas'} />
                    <FlatList data={this.state.reservasProduto}
                        keyExtractor={item => `${item.NUMERO}`}
                        renderItem={({ item }) =>
                            <ReservaProduto
                                image={`${this.state.ip}:${this.state.porta}/imagePessoas/${item.CODIGOPESSOA}.png`}
                                pessoa={item.PESSOA}
                                tipoReserva={item.TIPO_RESERVA}
                                documento={item.NUMERO}
                                almoxarifado={item.ALMOXARIFADO}
                                quantidade={item.QTD_RESERVA}
                            />
                        } />


                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}>Footer</Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
})