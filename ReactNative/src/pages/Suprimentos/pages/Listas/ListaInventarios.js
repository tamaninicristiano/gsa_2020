import React, { Component } from 'react'
import { StyleSheet, View, FlatList, Modal, Alert, TextInput, TouchableOpacity, Platform, Text } from 'react-native'
import Header from '../../../../components/Template/Header'
import Footer from '../../../../components/Template/Footer'
import Body from '../../../../components/Template/Body'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import Icon from 'react-native-vector-icons/FontAwesome'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import Inventario from '../../components/Inventario'

export default class Menu extends Component {
    state = {
        ip: "",
        porta: "",
        showModal: false,
        inventarios: [],
        date: new Date(),
        showDatePicker: false,
        observacoes: ''
    }

    OpenPage = (tela, item) => {
        this.props.navigation.navigate(tela, item);
    };

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.loadInventarios()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    loadInventarios = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/inventarios`)
            this.setState({ inventarios: res.data })
        } catch (err) {
            showError(err)
        }
    }

    onSave = async () => {
        try {
            await axios.post(`${this.state.ip}:${this.state.porta}/inventario`, {
                DATA: this.state.date,
                OBSERVACOES: this.state.observacoes
            })

            await Alert.alert('Sucesso!', 'Inventário registrado.')
            await this.setState({showModal: false})
            await this.loadInventarios()
        } catch (err) {
            Alert.alert(`${err}`)
        }
    }

    deleteInventario = async (ID, SITUACAO) => {
        //Alert.alert(`${SITUACAO}`)
        if (SITUACAO === 1) {
            try {
                await axios.delete(`${this.state.ip}:${this.state.porta}/inventario/${ID}`)
                await this.loadInventarios()
            } catch (err) {
                Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
            }
        } else {
            Alert.alert('Atenção', 'Somente é possível excluir na situação de cadastrado!')
        }
    }

    PerguntaDelete = async (msg, ID, SITUACAO) => {
        //await Alert.alert(`${SITUACAO}`)
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Deletar',
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.deleteInventario(ID, SITUACAO) },
            ],
            { cancelable: false },
        );
    }

    PerguntaCancel= async (msg, ID, SITUACAO) => {
        //await Alert.alert(`${SITUACAO}`)
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.cancelarInventario(ID, SITUACAO) },
            ],
            { cancelable: false },
        );
    }

    PerguntaConfirmar = async (msg, ID) => {
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Cancelar',
                    onPress: () => this.perguntaCancel(),
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.confirmaInventario(ID) },
            ],
            { cancelable: false },
        );
    }

    getDatePicker = () => {
        let datePicker = <DateTimePicker
        value={this.state.date}
        onChange={(_,date) => this.setState({date, showDatePicker: false})}
        mode='date'
        />

        const dateString = moment(this.state.date).format('DD/MM/YYYY')

        if(Platform.OS === 'android') {
            datePicker = (
                <View>
                    <TouchableOpacity onPress={() => this.setState({showDatePicker: true})}>
                        <Text style={styles.date}>
                            {dateString}
                        </Text>
                    </TouchableOpacity>
                    {this.state.showDatePicker && datePicker}
                </View>
            )
        }  
        return datePicker
    }

    confirmaInventario = async (ID) => {
        try {
            await axios.post(`${this.state.ip}:${this.state.porta}/inventarioSituacao/${ID}`, {
                SITUACAO: 2
            })
            await this.loadInventarios()
        } catch (err) {
            Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
        }
    }

    cancelarInventario = async (ID) => {
        try {
            await axios.post(`${this.state.ip}:${this.state.porta}/inventarioSituacao/${ID}`, {
                SITUACAO: 3
            })
            await this.loadInventarios()
        } catch (err) {
            Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
        }
    }

    addItens = (item) => {
        this.OpenPage('ListaInventarioItens', item)
    }

    render() {
        return (
            <View style={styles.container}>
                    <Modal style={styles.modal} visible={this.state.showModal}
                    animationType='fade'>
                    <View style={styles.containerFiltro}>
                        <Header />
                        <Body img={ImagemBack}>
                            <TitleScreen title='Cadastro Inventário' />
                            <View style={styles.containerForm}>
                                <View style={styles.input}>
                                    <View style={styles.Icon}>
                                        <Icon name='hourglass-half' size={20} color='#000000' />
                                    </View>
                                    <View style={{ width: '80%', alignItems: 'center' }}>
                                    {this.getDatePicker()}
                                    </View>
                                </View>
                                <View style={styles.inputTexto}>
                                    <View style={styles.Icon}>
                                        <Icon name='stack-overflow' size={20} color='#000000' />
                                    </View>
                                    <View style={{ width: '80%', alignItems: 'center' }}>
                                        <TextInput placeholder='Observações'
                                            multiline={true}
                                            onChangeText={obs => this.setState({ observacoes: obs })}
                                        />
                                    </View>
                                </View>
                            </View>
                        </Body>
                        <Footer>
                        <ButtonsTreeFooter
                            onClickButtonLeft={() => {this.setState({showModal: false})}}
                            onClickButtonCenter={() => this.onSave()}
                            onClickButtonRight={() => this.loadInventarios()}

                            IconLeft='close'
                            IconPrincipal='save'
                            IconRight='arrow-left'

                            colorLeft='#8A0808'
                            colorPrincipal='#000000'
                            colorRight='#298A08'
                        />
                    </Footer>
                    </View>
                </Modal >
                <View style={styles.Header} > 
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <FlatList data={this.state.inventarios}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) => 
                        <TouchableOpacity onPress={() => this.addItens(item)}>
                            <Inventario id={item.ID} 
                                        data={item.DATA} 
                                        idSituacao={item.ID_SITUACAO} 
                                        situacao={item.SITUACAO} 
                                        onCancelar={this.PerguntaCancel} 
                                        onDelete={this.PerguntaDelete}
                                        onConfirmar={this.PerguntaConfirmar}/>
                        </TouchableOpacity>
                        }/>
                </Body>
                <Footer>
                <ButtonsTreeFooter
                       onClickButtonCenter={() => {this.setState({showModal: true})}}
                       onClickButtonRight={() => this.loadInventarios()}
                       
                       IconLeft='close'
                       IconPrincipal='plus'
                       IconRight='refresh'

                       colorLeft='#8A0808'
                       colorPrincipal='#000000'
                       colorRight='#298A08'
                   />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    modal: {
        flex: 1
    },
    containerFiltro: {
        flex: 1
    },
    containerForm: {
        justifyContent: "center",
        alignItems: 'center'
    },
    Icon: {
        width: '10%', justifyContent: 'center', alignItems: 'center'
    },
    input: {
        height: 50,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    inputTexto: {
        height: 300,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    Card: {
        marginLeft: 15,
        marginRight: 15,
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    foto: {
        height: 220,
        width: 220
    },
    InfAdicional: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 80
    },
    fotoPessoa: {
        height: 150,
        width: 150
    },
    imagePessoa: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        height: 55,
        width: 55
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    date: {
        fontSize: 20
    }

})
