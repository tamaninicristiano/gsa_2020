import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, Image, TouchableOpacity, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import axios from 'axios'

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'
import ImagBack from '../../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'
import ImageCard from '../../../../components/Estrutura/ImageCard'
import QRCode from '../../../Global/components/qrCode'
import ModalFiltroProduto from '../../../Global/Filtros/ModalFiltroProduto'

import AsyncStorage from '@react-native-community/async-storage'

export default class SolicitacaoCompraItem extends Component {
    state = {
        ip: '',
        porta: '',
        placeHolderCodigo: 'Código do Produto:',
        placeHolderQuantidade: 'Quantidade:',
        codigoFiltro: null,
        showQRCodeModal: false,
        showFiltroModal: false,

        ID: null,
        DOCUMENTO_ID: null,
        DATAENTRADA: null,
        DOCUMENTO: null,
        CODIGOPESSOA: null,
        PESSOA: null,
        PRODUTO_ID: null,
        CODIGO: null,
        PRODUTO: null,
        QUANTIDADE: null,
        DATACONSULTA: null,
        SITUACAO: null,
        ORDEMCOMPRA: null
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregarEdit()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregarEdit = async () => {
        try {
            const params = await this.props.navigation.state.params;
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/solicitacaoCompraItem/${params.ID}`)
            await this.setState({
                ID: res.data[0].ID,
                PRODUTO_ID: res.data[0].PRODUTO_ID,
                CODIGO: res.data[0].CODIGO,
                PRODUTO: res.data[0].PRODUTO,
                QUANTIDADE: res.data[0].QUANTIDADE,
                DATACONSULTA: res.data[0].DATACONSULTA,
                SITUACAO: res.data[0].SITUACAO,
                ORDEMCOMPRA: res.data[0].ORDEMCOMPRA,
                codigoFiltro: res.data[0].CODIGO,
                placeHolderCodigo: `${res.data[0].CODIGO}`,
                placeHolderQuantidade: `${res.data[0].QUANTIDADE}`
            })

            const resDoc = await axios.get(`${this.state.ip}:${this.state.porta}/infoAdiconalProduto/${this.state.codigoFiltro}`)
            this.setState({
                DOCUMENTO_ID: resDoc.data[0].DOCUMENTO_ID,
                DATAENTRADA: resDoc.data[0].DATAENTRADA,
                DOCUMENTO: resDoc.data[0].DOCUMENTO,
                CODIGOPESSOA: resDoc.data[0].CODIGOPESSOA,
                PESSOA: resDoc.data[0].PESSOA,
                QUANTIDADE: resDoc.data[0].QUANTIDADE
            })
        } catch (err) {
            //Alert.alert('Atenção', 'Fornecedor não encontrado.')
        }
    }

    Sair = async () => {
        await this.props.navigation.goBack()
    }

    onPress = async () => {
        if (this.state.codigoFiltro != '') {
            try {
                const resP = await axios.get(`${this.state.ip}:${this.state.porta}/produto/${this.state.codigoFiltro}`)
                this.setState({
                    PRODUTO_ID: resP.data[0].ID,
                    CODIGO: resP.data[0].CODIGO,
                    PRODUTO: resP.data[0].PRODUTO,
                    DATACONSULTA: resP.data[0].DATACONSULTA
                })

                const res = await axios.get(`${this.state.ip}:${this.state.porta}/infoAdiconalProduto/${this.state.codigoFiltro}`)
                this.setState({
                    DOCUMENTO_ID: res.data[0].DOCUMENTO_ID,
                    DATAENTRADA: res.data[0].DATAENTRADA,
                    DOCUMENTO: res.data[0].DOCUMENTO,
                    CODIGOPESSOA: res.data[0].CODIGOPESSOA,
                    PESSOA: res.data[0].PESSOA,
                    QUANTIDADE: res.data[0].QUANTIDADE
                })
            } catch (err) {

            }
        }
    }

    onSave = async () => {
        if (this.state.CODIGO != '999999' & this.state.CODIGO != '' & this.state.CODIGO != null) {
            if (this.state.QUANTIDADE) {
                try {
                    await axios.post(`${this.state.ip}:${this.state.porta}/solicitacaoCompraItens`, {
                        ID: this.state.ID,
                        DATA: this.state.DATACONSULTA,
                        ITEM: this.state.PRODUTO_ID,
                        QUANTIDADE: this.state.QUANTIDADE,
                        CODIGO: this.state.CODIGO,
                        ORDEMCOMPRA: this.state.ORDEMCOMPRA
                    })

                    await Alert.alert('Sucesso!', 'Solicitação registrada.')
                    await this.Sair()
                } catch (err) {
                    Alert.alert('Erro', `${err}`)
                }
            } else {
                Alert.alert('Atenção!', 'Informe a quantidade!')
            }
        } else {
            Alert.alert('Atenção!', 'Produto não existe!')
        }
    }

    ShowQRCode = async () => {
        await this.setState({ showQRCodeModal: true })
    }

    ShowFiltro = async () => {
        await this.setState({ showFiltroModal: true })
    }

    FiltroProduto = async FiltroProduto => {
        //await Alert.alert('Dados', `${FiltroProduto}`)
        await this.setState({ showQRCodeModal: false })
        await this.setState({ showFiltroModal: false })
        await this.setState({ codigoFiltro: FiltroProduto })
        this.setState({ placeHolderCodigo: `${FiltroProduto}` })
        await this.onPress()
    }

    render() {
        return (
            <View style={styles.container}>
                <QRCode isVisible={this.state.showQRCodeModal}
                    onCancel={() => this.setState({ showQRCodeModal: false })}
                    onFiltrar={this.FiltroProduto}
                />
                <ModalFiltroProduto
                    isVisible={this.state.showFiltroModal}
                    onCancel={() => this.setState({ showFiltroModal: false })}
                    onFiltrar={this.FiltroProduto}
                />
                <Header />
                <Body img={ImagBack}>
                    <TitleScreen title='Solicitação de Compra' />
                    <View style={styles.containerForm}>
                        <View style={styles.input}>
                            <View style={styles.Icon}>
                                <Icon name='archive' size={20} color='#000000' />
                            </View>
                            <View style={{ width: '80%', alignItems: 'center' }}>
                                <TextInput keyboardType='numeric' placeholder={this.state.placeHolderCodigo}
                                    onChangeText={codigo => this.setState({ codigoFiltro: codigo })}
                                    onEndEditing={this.onPress} textAlign='center'
                                />
                            </View>
                            <View style={styles.Icon}>
                                <TouchableOpacity onPress={() => this.setState({ showFiltroModal: true })}>
                                    <Icon name='search' size={20} color='#000000' />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.input}>
                            <View style={styles.Icon}>
                                <Icon name='check-square-o' size={20} color='#000000' style={{ marginLeft: 15, width: 25 }} />
                            </View>
                            <View style={{ width: '80%', alignItems: "center" }}>
                                <TextInput keyboardType='numeric' placeholder={this.state.placeHolderQuantidade} textAlign='center'
                                    onChangeText={quantidade => this.setState({ QUANTIDADE: quantidade })}
                                    name='txtQuantidade'
                                />
                            </View>
                            <View style={{ width: '10%', }} />
                        </View>
                        <View style={styles.input}>
                            <View style={styles.Icon}>
                                <Icon name='shopping-cart' size={20} color='#000000' style={{ marginLeft: 15, width: 25 }} />
                            </View>
                            <View style={{ width: '80%', alignItems: "center" }}>
                                <TextInput keyboardType='numeric' placeholder='Nº Ordem de Compra:' textAlign='center'
                                    onChangeText={ordemCompra => this.setState({ ORDEMCOMPRA: ordemCompra })}
                                    name='txtQuantidade'
                                />
                            </View>
                            <View style={{ width: '10%', }} />
                        </View>
                    </View>
                    <ImageCard style={styles.Card}>
                        <View style={styles.containerCard}>
                            <View style={styles.containerCardHeader}>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                                    {this.state.CODIGO} -  {this.state.PRODUTO}
                                </Text>
                            </View>
                            <View style={styles.containerCardBody}>
                                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/image/${this.state.CODIGO}.png` }} style={styles.foto} />
                            </View>
                        </View>
                    </ImageCard>
                    <TitleScreen style={{ marginBottom: 5, marginTop: -10 }} title='Informações Adicionais' />
                    <View style={styles.InfAdicional}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text>Data Última Compra: {this.state.DATAENTRADA}</Text>
                            <Text>Documento: {this.state.DOCUMENTO}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text>Qtd. Última Compra: {this.state.QUANTIDADE}</Text>
                            <Text>Média Mensal Venda: </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%', paddingTop: 50 }}>
                            <View style={styles.imagePessoa}>
                                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/imagePessoas/${this.state.CODIGOPESSOA}.png` }} style={styles.fotoPessoa} />
                            </View>
                            <View style={{ width: '55%', paddingLeft: 60 }}>
                                <Text>{this.state.CODIGOPESSOA} - {this.state.PESSOA}</Text>
                            </View>
                        </View>
                    </View>
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonRight={this.onSave}
                        onClickButtonCenter={this.ShowQRCode}
                        onClickButtonLeft={this.Sair}

                        IconLeft='close'
                        IconPrincipal='qrcode'
                        IconRight='save'

                        colorLeft='#8A0808'
                        colorPrincipal='#000000'
                        colorRight='#298A08'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    containerForm: {
        justifyContent: "center",
        alignItems: 'center'
    },
    Icon: {
        width: '10%', justifyContent: 'center', alignItems: 'center'
    },
    input: {
        height: 50,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    Card: {
        marginLeft: 15,
        marginRight: 15,
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    foto: {
        height: 220,
        width: 220
    },
    InfAdicional: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 80
    },
    fotoPessoa: {
        height: 150,
        width: 150
    },
    imagePessoa: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        height: 55,
        width: 55
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
