import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, Alert, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../../../components/Template/Header'
import Footer from '../../../../components/Template/Footer'
import Body from '../../../../components/Template/Body'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import ImageCard from '../../../../components/Estrutura/ImageCard'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'

import TabGeralProduto from '../../components/TabGeralProduto'
import TabFornProduto from '../../components/TabFornProduto'
import TabCustosProduto from '../../components/TabCustosProduto'
import TabFiscalProduto from '../../components/TabFiscalProduto'

import axios from 'axios'
import QRCode from '../../../Global/components/qrCode'

const heightWindow = Dimensions.get('window').height

export default class ProdutoDetalhes extends Component {
    state = {
        ip: '',
        porta: '',

        tabSelect: 1,
        tabRender: null,
        tabGeralColor: '#ADADAD',
        tabFornecedoresColor: '',
        tabCustosColor: '',
        tabFiscalColor: '',
        showQRCodeModal: false,

        fornecedores: [],

        ID: 0,
        CODIGO: null,
        PRODUTO: null,
        ESPECIFICACOES: null,
        PRECOVENDA: null,
        PRATELEIRA: null,
        POSICAOPRATELEIRA: null,
        PESO: null,
        UNIDADE: null,
        VALORCUSTO: null,
        VALORMAIORCOMPRA: null,
        VALORMENORCOMPRA: null,
        VALORULTIMACOMPRA: null,
        DISPONIVEL: null,
        PREVISTO: null,
        RESERVADO: null,
        NCM: null,
        NCMNOME: null,
        TIPOITEM: null,
        VALORUNITARIO: null,
        MARCA: null,
        IMC: null,
        CUSTOTOTAL: null
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.tabActive()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    tabActive = () => {
        const tabGeral = <TabGeralProduto
            precoVenda={this.state.PRECOVENDA}
            disponivel={this.state.DISPONIVEL}
            previsto={this.state.PREVISTO}
            reservado={this.state.RESERVADO}
            prateleira={this.state.PRATELEIRA}
            posicaoPrateleira={this.state.POSICAOPRATELEIRA}
            peso={this.state.PESO}
            marca={this.state.MARCA}
            unidade={this.state.UNIDADE}
            especificacoes={this.state.ESPECIFICACOES}
            onClick={this.OpenPageReserva}
        />

        const tabFornecedor = <TabFornProduto
            fornecedores={this.state.fornecedores}
            onClick={this.OpenPagePessoa}
        />
        const tabCusto = <TabCustosProduto
            valorCusto={this.state.VALORCUSTO}
            valorCustoOperacional={this.state.VALORCUSTOPRODUCAO}
            custoTotal={this.state.CUSTOTOTAL}
            maiorCusto={this.state.VALORMAIORCOMPRA}
            menorCusto={this.state.VALORMENORCOMPRA}
            ultimoCusto={this.state.VALORULTIMACOMPRA}
            imc={this.state.IMC}
        />
        const tabFiscal = <TabFiscalProduto
            ncm={this.state.NCM}
            ncmNome={this.state.NCMNOME}
            tipoItem={this.state.TIPOITEM}
        />

        if (this.state.tabSelect === 1) {
            return this.setState({ tabRender: tabGeral })
        } else if (this.state.tabSelect === 2) {
            return this.setState({ tabRender: tabFornecedor })
        } else if (this.state.tabSelect === 3) {
            return this.setState({ tabRender: tabCusto })
        } else if (this.state.tabSelect === 4) {
            return this.setState({ tabRender: tabFiscal })
        } else {
            this.setState({ tabRender: tabGeral })
        }
    }

    clickGeral = async () => {
        await this.setState({
            tabSelect: 1,
            tabGeralColor: '#ADADAD',
            tabFornecedoresColor: null,
            tabCustosColor: null,
            tabFiscalColor: null
        })
        await this.tabActive()
    }

    clickFornecedores = async () => {
        await this.setState({
            tabSelect: 2,
            tabGeralColor: null,
            tabFornecedoresColor: '#ADADAD',
            tabCustosColor: null,
            tabFiscalColor: null
        })
        await this.tabActive()
    }

    clickCustos = async () => {
        await this.setState({
            tabSelect: 3,
            tabGeralColor: null,
            tabFornecedoresColor: null,
            tabCustosColor: '#ADADAD',
            tabFiscalColor: null
        })
        await this.tabActive()
    }

    clickFiscal = async () => {
        await this.setState({
            tabSelect: 4,
            tabGeralColor: null,
            tabFornecedoresColor: null,
            tabCustosColor: null,
            tabFiscalColor: '#ADADAD'
        })
        await this.tabActive()
    }

    ShowQRCode = async () => {
        await this.setState({ showQRCodeModal: true })
    }

    Atualizar = async () => {
        try {
            const res = await axios.get(`/${this.state.ip}:${this.state.porta}/produto/${this.state.CODIGO}`)
            await this.setState({
                ID: res.data[0].ID,
                CODIGO: res.data[0].CODIGO,
                PRODUTO: res.data[0].PRODUTO,
                PRATELEIRA: res.data[0].PRATELEIRA,
                POSICAOPRATELEIRA: res.data[0].POSICAOPRATELEIRA,
                PESO: res.data[0].PESO,
                UNIDADE: res.data[0].UNIDADE,
                VALORCUSTO: res.data[0].VALORCUSTO,
                VALORMAIORCOMPRA: res.data[0].VALORMAIORCOMPRA,
                VALORMENORCOMPRA: res.data[0].VALORMENORCOMPRA,
                VALORULTIMACOMPRA: res.data[0].VALORULTIMACOMPRA,
                DISPONIVEL: res.data[0].DISPONIVEL,
                PREVISTO: res.data[0].PREVISTO,
                RESERVADO: res.data[0].RESERVADO,
                NCM: res.data[0].NCM,
                NCMNOME: res.data[0].NCMNOME,
                TIPOITEM: res.data[0].TIPOITEM,
                PRECOVENDA: res.data[0].PRECOVENDA,
                MARCA: res.data[0].MARCA,
                IMC: res.data[0].IMC,
                CUSTOTOTAL: res.data[0].CUSTOTOTAL
            })

            const resF = await axios.get(`${this.state.ip}:${this.state.porta}/produtoFornecedores/${this.state.ID}`)
            //Alert.alert('ID', `${this.state.ID}`)
            await this.setState({
                fornecedores: resF.data
            })

            await this.tabActive()
        } catch (err) {
            Alert.alert('Atenção', 'Produto não encontrado.')
        }
    }

    FiltroProduto = async FiltroProduto => {
        await this.setState({ showQRCodeModal: false })
        if (FiltroProduto == null) {
            Alert.alert('Atenção', 'Produto não encontrado!')
        } else {
            try {
                const res = await axios.get(`${this.state.ip}:${this.state.porta}/produto/${FiltroProduto}`)
                await this.setState({
                    ID: res.data[0].ID,
                    CODIGO: res.data[0].CODIGO,
                    PRODUTO: res.data[0].PRODUTO,
                    ESPECIFICACOES: res.data[0].ESPECIFICACOES,
                    PRATELEIRA: res.data[0].PRATELEIRA,
                    POSICAOPRATELEIRA: res.data[0].POSICAOPRATELEIRA,
                    PESO: res.data[0].PESO,
                    UNIDADE: res.data[0].UNIDADE,
                    VALORCUSTO: res.data[0].VALORCUSTO,
                    VALORMAIORCOMPRA: res.data[0].VALORMAIORCOMPRA,
                    VALORMENORCOMPRA: res.data[0].VALORMENORCOMPRA,
                    VALORULTIMACOMPRA: res.data[0].VALORULTIMACOMPRA,
                    DISPONIVEL: res.data[0].DISPONIVEL,
                    PREVISTO: res.data[0].PREVISTO,
                    RESERVADO: res.data[0].RESERVADO,
                    NCM: res.data[0].NCM,
                    NCMNOME: res.data[0].NCMNOME,
                    TIPOITEM: res.data[0].TIPOITEM,
                    PRECOVENDA: res.data[0].PRECOVENDA,
                    MARCA: res.data[0].MARCA,
                    IMC: res.data[0].IMC,
                    CUSTOTOTAL: res.data[0].CUSTOTOTAL
                })

                const resF = await axios.get(`${this.state.ip}:${this.state.porta}/fornecedoresProduto/${this.state.ID}`)
                //Alert.alert('ID', `${this.state.ID}`)
                await this.setState({
                    fornecedores: resF.data
                })

                await this.tabActive()
            } catch (err) {
                Alert.alert('Atenção', 'Produto não encontrado.')
            }
        }
    }

    OpenPagePessoa = (item) => {
        this.props.navigation.navigate("Pessoa", item);
    }

    OpenPageReserva = () => {
        this.props.navigation.navigate("ListaReservasProduto", this.state.CODIGO);
    }

    OpenPageComponentesProduto = () => {
        if (this.state.CODIGO) {
            this.props.navigation.navigate("ListaComponentesProduto", this.state.CODIGO);
        } else {
            Alert.alert('Atenção', "Produto não selecionado!")
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <QRCode isVisible={this.state.showQRCodeModal}
                    onCancel={() => this.setState({ showQRCodeModal: false })}
                    onFiltrar={this.FiltroProduto}
                />
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={'Detalhes do Produto'} />
                    <ImageCard>
                        <View style={styles.containerCard}>
                            <View style={styles.containerCardHeader}>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                                    {this.state.CODIGO} -  {this.state.PRODUTO}
                                </Text>
                            </View>
                            <View style={styles.containerCardBody}>
                                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/image/${this.state.CODIGO}.png` }} style={styles.foto} />
                            </View>
                        </View>
                    </ImageCard>
                    <View style={styles.tabView}>
                        <TouchableOpacity
                            onPress={() => this.clickGeral()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabGeralColor }]}>
                            <Icon name={'book'} size={25} />
                            <Text style={styles.subTitleTabButton}>Geral</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickFornecedores()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabFornecedoresColor }]}>
                            <Icon name={'users'} size={25} />
                            <Text style={styles.subTitleTabButton}>Fornecedores</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickCustos()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabCustosColor }]}>
                            <Icon name={'pie-chart'} size={25} />
                            <Text style={styles.subTitleTabButton}>Custos</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickFiscal()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabFiscalColor }]}>
                            <Icon name={'balance-scale'} size={25} />
                            <Text style={styles.subTitleTabButton}>Fiscal</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerTabBody}>
                        {
                            this.state.tabRender
                        }
                    </View>
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonRight={this.OpenPageComponentesProduto}
                        onClickButtonCenter={this.ShowQRCode}
                        onClickButtonLeft={this.Atualizar}

                        IconLeft='refresh'
                        IconPrincipal='qrcode'
                        IconRight='sitemap'

                        colorLeft='#000000'
                        colorPrincipal='#000000'
                        colorRight='#000000'
                    />
                </Footer>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    foto: {
        width: heightWindow > 1000 ? 280 : 220,
        height: heightWindow > 1000 ? 280 : 220,
        resizeMode: 'center',
    },
    containerButtonsImage: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    tabView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        height: 55,
    },
    tabViewButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ADADAD",
        width: '25%',
        opacity: 0.5
    },
    subTitleTabButton: {
        fontSize: 12
    },
    containerBodyTabView: {
        flexDirection: 'row',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        width: '100%',
        height: '42%',
        backgroundColor: '#FFF'
    },

    scene: {
        flex: 1,
    },
    containerTabBody: {
        backgroundColor: '#FFF',
        flex: 1,
        flexDirection: 'row'
    }
})