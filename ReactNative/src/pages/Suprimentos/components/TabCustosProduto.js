import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Dimensions } from 'react-native'

const widthWindow = Dimensions.get("window").width

export default class TabCustosProduto extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ marginBottom: 5 }}>
                    <View style={styles.containerFilho}>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>R$ Custo:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.valorCusto}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>R$ Operacional</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.valorCustoOperacional}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>R$ Custo Total:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { color: '#B40404', fontSize: 18, fontWeight: 'bold' }]}>{this.props.custoTotal}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>R$ Maior Custo:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.maiorCusto}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>R$ Menor Custo:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.menorCusto}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>R$ Ultimo Custo:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={styles.labelProps}>{this.props.ultimoCusto}</Text>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>IMC:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={styles.labelProps}>{this.props.imc}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#AAA',
    },
    containerFilho: {
        width: widthWindow,
        padding: 7
    },
    inputText: {
        marginTop: 10,
        height: 'auto',
        maxHeight: 55,
        minHeight: 50,
        backgroundColor: '#FFF',
        borderRadius: 12,
    },
    containerLabelIcon: {
        paddingLeft: 15,
        flexDirection: 'row',
    },
    containerLabelValor: {
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold', fontSize: 15
    },
    labelProps: {
        fontSize: 15.5
    }
})
