import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const widthWindow = Dimensions.get("window").width

export default class TabGeralProduto extends Component {

    onClickPage = async () => {
        await this.props.onClick()
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.containerFilho}>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>R$ Preço de Venda</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { color: '#088A08', fontSize: 18, fontWeight: 'bold' }]}>{this.props.precoVenda}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.3 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Medida:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.unidade}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.65 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Disponível:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.disponivel}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Previsto:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.previsto}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.475 }]}>
                                <TouchableOpacity onPress={() => this.onClickPage()}>
                                    <View style={styles.containerLabelIcon}>
                                        <Text style={styles.label}>Reservado:</Text>
                                    </View>
                                    <View style={[styles.containerLabelValor, { justifyContent: 'space-between', marginLeft: 20 }]}>
                                        <Icon name='share-square-o' size={15} />
                                        <Text style={styles.labelProps}>{this.props.reservado}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Prateleira:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.prateleira}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Posição:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.posicaoPrateleira}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Peso</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={styles.labelProps}>{this.props.peso} Kg</Text>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Marca</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={styles.labelProps}>{this.props.marca}</Text>
                            </View>
                        </View>
                        <View style={[styles.inputText]}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Especificações</Text>
                            </View>
                            <View style={[styles.containerLabelValor]}>
                                <Text style={styles.labelProps}>{this.props.especificacoes}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#AAA',
    },
    containerFilho: {
        flex: 1,
        width: widthWindow,
        padding: 7
    },
    inputText: {
        marginTop: 10,
        height: 'auto',
        minHeight: 50,
        backgroundColor: '#FFF',
        borderRadius: 12,
    },
    containerLabelIcon: {
        paddingLeft: 15,
        flexDirection: 'row',
    },
    containerLabelValor: {
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold', fontSize: 15
    },
    labelProps: {
        fontSize: 15.5
    }
})
