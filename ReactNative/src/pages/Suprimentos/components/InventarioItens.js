import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    const getLeftContent = () => {
        return (
            <TouchableOpacity style={styles.Left}
                onPress={() => props.onConfirmar && props.onConfirmar('Deseja realmente Confirmar?', props.id)}>
                <Icon name="check" size={30} color="#FFF" />
            </TouchableOpacity>
        )
    }

    const botaoDireta = () => {
        if(props.idSituacao === 1) {
           return (
                <TouchableOpacity style={styles.Right}
                    onPress={() => props.onDelete && props.onDelete('Deseja realmente excluir o inventário?', props.id, props.idSituacao)}>
                    <Icon name="trash-o" size={30} color="#FFF" />
                </TouchableOpacity>
           )
        } else {
            return (
                <TouchableOpacity style={styles.Right}
                    onPress={() => props.onCancelar && props.onCancelar('Deseja realmente cancelar o inventário?', props.id, props.idSituacao)}>
                    <Icon name="remove" size={30} color="#FFF" />
                </TouchableOpacity>
            )
        }
    }

    return (
        <Swipeable
            renderLeftActions={getLeftContent}
            renderRightActions={botaoDireta}>
            <View style={[styles.container]}>
                <View style={styles.containerImage}>
                    <Image source={{ uri: props.image }} style={styles.foto} />
                </View>
                <View style={styles.containerInfo}>
                    <View style={styles.ContainerTitle}>
                        <Text style={styles.title}>{props.produto}</Text>
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Código: {props.codigo}</Text>
                        <Text style={styles.Subtitle}>Disp.: {props.disponivel}</Text>
                        <Text style={styles.Subtitle}>Dif.: {props.diferenca}</Text>
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Qtd.: {props.quantidade}</Text>
                        <Text style={styles.Subtitle}>Custo: {props.custo}</Text>
                        <Text style={styles.Subtitle}>C. Total: {props.custototal}</Text>
                    </View>
                </View>
            </View>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '80%'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%',
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    },
    Left: {
        backgroundColor: '#21610B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 20
    },
    Right: {
        backgroundColor: '#610B0B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 25
    }

})