import React, { Component } from 'react'
import { StyleSheet, View, FlatList, TouchableOpacity, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import FornecedorLista from '../components/FornecedorLista'

export default class TabFornProduto extends Component {
    state = {
        ip: '',
        porta: '',
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    onClickPage = (item) => {
        this.props.navigation.navigate("Pessoa", item);
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.fornecedores}
                    keyExtractor={item => `${item.ID}`}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => this.onClickPage(item)}>
                            <FornecedorLista codigo={item.CODIGO}
                                nome={item.FORNECEDOR}
                                cnpjcpf={item.CNPJCPF}
                                image={`${this.state.ip}:${this.state.porta}/imagePessoas/${item.CODIGO}.png`}
                            />
                        </TouchableOpacity>
                    } />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
