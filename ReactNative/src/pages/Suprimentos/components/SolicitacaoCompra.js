import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    const getLeftContent = () => {
        return (
            <TouchableOpacity style={styles.Left}
                onPress={() => props.onConfirmar && props.onConfirmar('Deseja realmente Confirmar?', props.id)}>
                <Icon name="check" size={30} color="#FFF" />
            </TouchableOpacity>
        )
    }

    const getRigthContent = () => {
        return (
            <TouchableOpacity style={styles.Right}
                onPress={() => props.onDelete && props.onDelete('Deseja realamente cancelar está solicitação?', props.id, props.situacao)}>
                <Icon name="trash-o" size={30} color="#FFF" />
            </TouchableOpacity>
        )
    }

    return (
        <Swipeable
            renderLeftActions={getLeftContent}
            renderRightActions={getRigthContent}>
            <View style={[styles.container]}>
                <View style={styles.containerImage}>
                    <Image source={{ uri: props.image }} style={styles.foto} />
                </View>
                <View style={styles.containerInfo}>
                    <View style={styles.ContainerTitle}>
                        <Text style={styles.title}>{props.produto}</Text>
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Código: {props.codigo}</Text>
                        <Text style={styles.Subtitle}>Quantidade: {props.quantidade}</Text>
                        <Text />
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Data Sol.: {props.data}</Text>
                        <Text style={styles.Subtitle}>O.C.: {props.ordemCompra}</Text>
                        <Text />
                    </View>
                </View>
                <View style={styles.IconSituacao}>
                    <Icon name='circle' size={20} color={props.situacaoCor} />
                </View>
            </View>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        width: '100%'
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '70%'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%'
    },
    IconSituacao: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '10%'
    },
    title: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
        fontWeight: 'bold'
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    },
    Left: {
        backgroundColor: '#21610B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 20
    },
    Right: {
        backgroundColor: '#610B0B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 25
    }

})