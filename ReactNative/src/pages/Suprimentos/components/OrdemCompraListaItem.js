import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={styles.containerImage}>
                <Icon name="circle" size={20} color="#000000" />
            </View>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.descricaoItem}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Previsão Entrega: {props.dataentrega}</Text>
                    <Text style={styles.Subtitle}>Qtd.: {props.codigoStuhlert}</Text>
                    <Text />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        opacity: 0.75,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 70,
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '80%'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%',
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    }

})