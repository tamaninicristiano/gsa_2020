import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    const getLeftContent = () => {
        return (
            <TouchableOpacity style={styles.Left}
                onPress={() => props.onConfirmar && props.onConfirmar('Deseja realmente Confirmar?', props.id)}>
                <Icon name="check" size={30} color="#FFF" />
            </TouchableOpacity>
        )
    }

    const botaoDireta = () => {
        if(props.idSituacao === 1) {
           return (
                <TouchableOpacity style={styles.Right}
                    onPress={() => props.onDelete && props.onDelete('Deseja realmente excluir o inventário?', props.id, props.idSituacao)}>
                    <Icon name="trash-o" size={30} color="#FFF" />
                </TouchableOpacity>
           )
        } else {
            return (
                <TouchableOpacity style={styles.Right}
                    onPress={() => props.onCancelar && props.onCancelar('Deseja realmente cancelar o inventário?', props.id, props.idSituacao)}>
                    <Icon name="remove" size={30} color="#FFF" />
                </TouchableOpacity>
            )
        }
    }

    return (
        <Swipeable
            renderLeftActions={getLeftContent}
            renderRightActions={botaoDireta}>
        <View style={[styles.container]}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>Data: {props.data}</Text>
                </View>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>Status: {props.situacao}</Text>
                </View>
        </View>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        opacity: 0.75,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 70
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5,
        margin: 7
    },
    title: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 17,
        fontWeight: 'bold'
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    },
    Left: {
        backgroundColor: '#21610B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 20
    },
    Right: {
        backgroundColor: '#610B0B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 25
    }

})