import React, { Component } from 'react'
import { Modal, View, StyleSheet, TouchableWithoutFeedback, Dimensions } from 'react-native'

import Input from '../../../components/Estrutura/Input'
import Button from '../../../components/Estrutura/Button'
import QRCodeScanner from 'react-native-qrcode-scanner';

const inicialState = { data: 0 }
const { width } = Dimensions.get('screen')

export default class QrCode extends Component {
    state = {
        ...inicialState
    }

    filtrar = async () => {
        await this.props.onFiltrar && this.props.onFiltrar(this.state.data)
        await this.setState({ ...inicialState })
    }

    onRead = async e => {
        await this.setState({ data: e.data })
        await this.filtrar()
    }

    render() {
        const leftTop = {
            borderLeftWidth: 2,
            borderTopWidth: 2,
            borderColor: 'green'
        }
        const leftBottom = {
            borderLeftWidth: 2,
            borderBottomWidth: 2,
            borderColor: 'green'
        }
        const rightTop = {
            borderRightWidth: 2,
            borderTopWidth: 2,
            borderColor: 'green'
        }
        const rightBottom = {
            borderRightWidth: 2,
            borderBottomWidth: 2,
            borderColor: 'green'
        }

        return (
            <Modal style={styles.container} transparent={true} visible={this.props.isVisible}
                onRequestClose={this.props.onCancel}
                animationType='fade'>
                <View style={{ backgroundColor: '#AAA', height: '55%' }}>
                    <View style={{ backgroundColor: '#FFF', height: '65%' }}>
                        <View style={{ ...StyleSheet.absoluteFill, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000' }}>
                            <QRCodeScanner onRead={this.onRead} />
                            <View style={{ width: width / 2, height: width / 2 }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1, ...leftTop }} />
                                    <View style={{ flex: 1 }} />
                                    <View style={{ flex: 1, ...rightTop }} />
                                </View>
                                <View style={{ flex: 1 }} />
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1, ...leftBottom }} />
                                    <View style={{ flex: 1 }} />
                                    <View style={{ flex: 1, ...rightBottom }} />
                                </View>
                            </View>

                        </View>
                    </View>
                    <View style={styles.containerDados}>
                        <View style={styles.inputDados}>
                            <Input keyboardType='numeric' icon="qrcode" style={styles.input}
                                onChangeText={codigo => this.setState({ data: codigo })} />
                        </View>
                        <View style={styles.containerButton}>
                            <Button nome='Confirmar' style={styles.buttonConfirmar} click={this.filtrar} />
                            <Button nome='Cancelar' click={this.props.onCancel} style={styles.buttonCancelar} />
                        </View>
                    </View>
                </View>

                <TouchableWithoutFeedback
                    onPress={this.props.onCancel}>
                    <View style={styles.backgroundInferior}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1
    },
    containerDados: {
        marginTop: 9,
        height: '45%',
        alignItems: 'center'
    },
    inputDados: {
        width: '90%'
    },
    backgroundInferior: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.75)'
    },
    input: {
        margin: 5,
        backgroundColor: '#FFF',
    },
    containerButton: {
        margin: 5,
        flexDirection: 'row',
        width: '90%',
        alignContent: 'center',
    },
    buttonConfirmar: {
        width: '70%'
    },
    buttonCancelar: {
        marginLeft: 5,
        width: '30%',
        backgroundColor: '#610B0B'
    },

})