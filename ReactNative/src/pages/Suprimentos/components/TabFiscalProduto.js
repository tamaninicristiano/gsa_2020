import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'

export default class TabCustosProduto extends Component {
    render() {
        return (
            <View style={[styles.container]}>
                <ScrollView style={{ marginBottom: 5 }}>
                    <View style={[styles.inputText, { height: 'auto', minHeight: 50, }]}>
                        <View style={styles.containerLabelIcon}>
                            <Text style={styles.label}>NCM:</Text>
                        </View>
                        <View style={[styles.containerLabelValor]}>
                            <Text style={[styles.labelProps, { paddingLeft: 20, paddingBottom: 5, textAlign: 'justify' }]}>
                                {this.props.ncm} - {this.props.ncmNome}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.inputText}>
                        <View style={styles.containerLabelIcon}>
                            <Text style={styles.label}>Tipo Item:</Text>
                        </View>
                        <View style={styles.containerLabelValor}>
                            <Text style={styles.labelProps}>{this.props.tipoItem}</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#AAA',
    },
    inputText: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        height: 'auto',
        maxHeight: 55,
        minHeight: 50,
        backgroundColor: '#FFF',
        borderRadius: 12,
    },
    containerLabelIcon: {
        paddingLeft: 15,
        flexDirection: 'row',
    },
    containerLabelValor: {
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold', fontSize: 15
    },
    labelProps: {
        fontSize: 15.5
    }
})
