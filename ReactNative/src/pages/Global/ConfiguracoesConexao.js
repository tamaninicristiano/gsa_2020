import React, { Component } from 'react'
import { StyleSheet, View, Alert, Keyboard, Dimensions, Text } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'

//Componentes de Template
import Header from '../../components/Template/Header'
import Body from '../../components/Template/Body'
import Footer from '../../components/Template/Footer'

//Componente de estrutura
import InputText from '../../components/Estrutura/AuthInput'
import TitleScreen from '../../components/Estrutura/TitleScreen'
import Button from '../../components/Estrutura/Button'

const Height = Dimensions.get('window').height
const Width = Dimensions.get('window').width

export default class ConfiguracoesConexao extends Component {
    state = {
        ip: '',
        porta: '',
        usuario: null
    }

    async componentDidMount() {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                // Alert.alert('Dados', dados.ip)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    salvar = async () => {
        try {
            let data = {
                ip: this.state.ip,
                porta: this.state.porta
            }
            await AsyncStorage.setItem('dadosServidor', JSON.stringify(data))

            Keyboard.dismiss();
            Alert.alert('Sucesso', 'Dados salvo com sucesso!')

        } catch (err) {
            Alert.alert('Erro', 'Falha no cadastro')
        }
    }

    testarConexao = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/usuario`)
            await this.setState({
                usuario: res.data[0].ID,
            })
            if (this.state.usuario !== 0) {
                Alert.alert('Sucesso!', 'Aplicativo conectado ao servidor!')
            }
        } catch (err) {
            Alert.alert('Atenção!', 'Aplicativo sem conexão com o servidor! Tente mais tarde!')
        }
    }

    render() {
        const validacoes = []
        validacoes.push(this.state.ip && this.state.ip.trim().length >= 12)
        validacoes.push(this.state.porta && this.state.porta.trim().length >= 3)

        const validForm = validacoes.reduce((t, a) => t && a)

        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title='Configurações de Conexão' />
                    <View>
                        <View style={styles.containerForm}>
                            <InputText icon="database" placeholder='Digite o IP do Servidor' style={styles.input} onChangeText={ip => this.setState({ ip })} />
                            <InputText icon="inbox" placeholder='Digite a porta de acesso' style={styles.input} onChangeText={porta => this.setState({ porta })} />
                        </View>
                        <View style={styles.containerBotao}>
                            <Button nome='Salvar' click={this.salvar} habilitar={!validForm} />
                        </View>
                        <View style={styles.containerBotao}>
                            <Button nome='Testar Conexão' click={this.testarConexao} />
                        </View>
                    </View>
                    <View>
                        <Text>Largura: {Width}</Text>
                        <Text>Altura: {Height}</Text>
                    </View>
                </Body>
                <Footer>
                    <View>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    containerForm: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        width: '90%',
        backgroundColor: '#FFF'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    containerBotao: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
})
