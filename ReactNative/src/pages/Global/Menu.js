import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'
import logo from '../../assets/img/LogoHeader.png'

export default class Menu extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name='bars' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <View style={styles.Body}>
                    <ImageBackground source={ImagemBack} style={styles.containerBody}>
                        <View style={styles.containerLogoCentral}>
                            <Image source={logo} style={styles.logo} />
                        </View>
                    </ImageBackground>
                </View>
                <Footer>
                    <View>

                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    Body: {
        flex: 1,
        width: '100%'
    },
    logo: {
        height: 35,
        width: 185,
        padding: 5
    },
    containerBody: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center'
    },
    containerLogoCentral: {

        backgroundColor: '#AAA',
        opacity: 0.8,
        height: 55,
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    }
})