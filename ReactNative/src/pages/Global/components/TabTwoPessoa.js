import React, { Component } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import ContatosLista from '../components/ContatosLista'

export default class TabTwoPessoa extends Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.contatos}
                    keyExtractor={item => `${item.ID}`}
                    renderItem={({ item }) => <ContatosLista nome={item.CONTATO} email={item.EMAIL} ddd={item.DDD} telefone={item.TELEFONE} />} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})
