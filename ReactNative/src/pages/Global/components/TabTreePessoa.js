import React, { Component } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import ProdutoListaFornecedor from '../components/ProdutoListaFornecedor'

export default class TabTreePessoa extends Component {
    state = {
        ip: '',
        porta: '',
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.produtosFornecedor}
                    keyExtractor={item => `${item.CODIGOPRODUTO}`}
                    renderItem={({ item }) => <ProdutoListaFornecedor codigo={item.CODIGOPRODUTO} image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGOSTUHLERT}.png`} codigoStuhlert={item.CODIGOSTUHLERT} descricaoFornecedor={item.DESCRICAOPRODFORNECEDOR} />} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
