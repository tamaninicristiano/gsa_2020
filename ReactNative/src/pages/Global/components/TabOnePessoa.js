import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Dimensions } from 'react-native'

const widthWindow = Dimensions.get("window").width

export default class TabOnePessoa extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.containerFilho}>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>CNPJ/CPF:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { color: '#000000', fontSize: 18, fontWeight: 'bold' }]}>{this.props.cnpjcpf}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.25 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Tipo Log.:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.tipologradouro}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.70 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Logradouro:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.logradouro}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Número:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.numero}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.475 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>CEP:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.cep}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.3 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>UF:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.uf}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.65 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Município:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.municipio}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: widthWindow * 0.3 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>DDD:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.ddd}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: widthWindow * 0.65 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Telefone:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.telefone}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Email:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={styles.labelProps}>{this.props.email}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#AAA',
    },
    containerFilho: {
        flex: 1,
        width: widthWindow,
        padding: 7
    },
    inputText: {
        marginTop: 10,
        height: 'auto',
        maxHeight: 55,
        minHeight: 50,
        backgroundColor: '#FFF',
        borderRadius: 12,
    },
    containerLabelIcon: {
        paddingLeft: 15,
        flexDirection: 'row',
    },
    containerLabelValor: {
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold', fontSize: 15
    },
    labelProps: {
        fontSize: 15.5
    }
})
