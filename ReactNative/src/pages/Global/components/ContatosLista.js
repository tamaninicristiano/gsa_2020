import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.nome}</Text>
                    <Text>Fone: {props.ddd} {props.telefone}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Email: {props.email}</Text>
                    <Text />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        opacity: 0.75,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 70,
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 5
    },
    containerInfo: {
        width: '80%',
        margin: 20
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    }

})