import React, { Component } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'

import OrdemCompraItens from './OrdemCompraItensLista'

export default class TabFourPessoa extends Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.ordemCompraItens}
                    keyExtractor={item => `${item.ID}`}
                    renderItem={({ item }) => <OrdemCompraItens
                        unidade={item.UNIDADE}
                        quantidade={item.QUANTIDADE}
                        descricaoFornecedor={item.PRODUTO}
                        codigoStuhlert={item.CODIGOSTUHLERT}
                        ordemCompra={item.ORDEMCOMPRA}
                        previsaoEntrega={item.PREVISAOENTREGA}
                        colorSituacao={item.SITUACAOCOR}
                        colorSituacaoBorder={item.SITUACAOCORBORDA} />}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})
