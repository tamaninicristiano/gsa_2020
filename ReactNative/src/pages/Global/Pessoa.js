import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, Alert, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Icon from 'react-native-vector-icons/FontAwesome'
import Icon5 from 'react-native-vector-icons/FontAwesome5'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'
import Body from '../../components/Template/Body'

import TabOnePessoa from './components/TabOnePessoa'
import TabTwoPessoa from './components/TabTwoPessoa'
import TabTreePessoa from './components/TabTreePessoa'
import TabFourPessoa from './components/TabFourPessoa'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'
import ImageCard from '../../components/Estrutura/ImageCard'
import TitleScreen from '../../components/Estrutura/TitleScreen'

const heightWindow = Dimensions.get('window').height

export default class Pessoa extends Component {
    state = {
        ip: '',
        porta: '',

        tabSelect: 1,
        tabRender: null,
        tabOneColor: '#ADADAD',
        tabTwoColor: '',
        tabTreeColor: '',
        tabFourColor: '',
        showQRCodeModal: false,

        produtosFornecedor: [],

        ordemCompraItens: [],

        //Dados da pessoa
        ID: null,
        CODIGO: null,
        CNPJCPF: null,
        NOME: null,
        TIPOLOGRADOURO: null,
        LOGRADOURO: null,
        NUMERO: null,
        CEP: null,
        UF: null,
        MUNICIPIO: null,
        DDD: null,
        TELEFONE: null,
        EMAIL: null,

        contatos: []
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    tabActive = () => {
        const tabOne = <TabOnePessoa
            cnpjcpf={this.state.CNPJCPF}
            tipologradouro={this.state.TIPOLOGRADOURO}
            logradouro={this.state.LOGRADOURO}
            numero={this.state.NUMERO}
            cep={this.state.CEP}
            uf={this.state.UF}
            municipio={this.state.MUNICIPIO}
            ddd={this.state.DDD}
            telefone={this.state.TELEFONE}
            email={this.state.EMAIL}
        />

        const tabTwo = <TabTwoPessoa
            contatos={this.state.contatos}
        />

        const tabTree = <TabTreePessoa
            produtosFornecedor={this.state.produtosFornecedor}
        />

        const tabFour = <TabFourPessoa
            ordemCompraItens={this.state.ordemCompraItens}
        />

        if (this.state.tabSelect === 1) {
            return this.setState({ tabRender: tabOne })
        } else if (this.state.tabSelect === 2) {
            return this.setState({ tabRender: tabTwo })
        } else if (this.state.tabSelect === 3) {
            return this.setState({ tabRender: tabTree })
        } else if (this.state.tabSelect === 4) {
            return this.setState({ tabRender: tabFour })
        } else {
            this.setState({ tabRender: tabOne })
        }
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.tabActive()
        await this.carregamento()

    }

    clickOne = async () => {
        await this.setState({
            tabSelect: 1,
            tabOneColor: '#ADADAD',
            tabTwoColor: null,
            tabTreeColor: null,
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickTwo = async () => {
        await this.setState({
            tabSelect: 2,
            tabOneColor: null,
            tabTwoColor: '#ADADAD',
            tabTreeColor: null,
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickTree = async () => {
        await this.setState({
            tabSelect: 3,
            tabOneColor: null,
            tabTwoColor: null,
            tabTreeColor: '#ADADAD',
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickFour = async () => {
        await this.setState({
            tabSelect: 4,
            tabOnelColor: null,
            tabTwoColor: null,
            tabTreeColor: null,
            tabFourColor: '#ADADAD'
        })
        await this.tabActive()
    }

    closed = (tela) => {
        this.props.navigation.navigate(tela);
    };


    carregamento = async () => {
        try {
            const params = await this.props.navigation.state.params;
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/pessoa/${params.ID}`)
            await this.setState({
                ID: res.data[0].ID,
                CODIGO: res.data[0].CODIGO,
                CNPJCPF: res.data[0].CNPJCPF,
                NOME: res.data[0].NOME,
                TIPOLOGRADOURO: res.data[0].TIPOLOGRADOURO,
                LOGRADOURO: res.data[0].LOGRADOURO,
                NUMERO: res.data[0].NUMERO,
                CEP: res.data[0].CEP,
                UF: res.data[0].UF,
                MUNICIPIO: res.data[0].MUNICIPIO,
                DDD: res.data[0].DDD,
                TELEFONE: res.data[0].TELEFONE,
                EMAIL: res.data[0].EMAIL
            })

            const resPC = await axios.get(`${this.state.ip}:${this.state.porta}/pessoaContatos/${params.ID}`)
            await this.setState({
                contatos: resPC.data
            })

            const resPF = await axios.get(`${this.state.ip}:${this.state.porta}/produtosFornecedor/${params.ID}`)
            await this.setState({
                produtosFornecedor: resPF.data
            })

            const resOCI = await axios.get(`${this.state.ip}:${this.state.porta}/ordemcompraitens/${params.ID}`)
            await this.setState({
                ordemCompraItens: resOCI.data
            })

            await this.tabActive()
        } catch (err) {
            Alert.alert('Atenção', 'Fornecedor não encontrado.')
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.closed('ProdutoDetalhes')}>
                        <Icon name='arrow-left' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={'Dados do Fornecedor'} />
                    <ImageCard>
                        <View style={styles.containerCard}>
                            <View style={styles.containerCardHeader}>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                                    {this.state.CODIGO} -  {this.state.NOME}
                                </Text>
                            </View>
                            <View style={styles.containerCardBody}>
                                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/imagePessoas/${this.state.CODIGO}.png` }} style={styles.foto} />
                            </View>
                        </View>
                    </ImageCard>
                    <View style={styles.tabView}>
                        <TouchableOpacity
                            onPress={() => this.clickOne()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabOneColor }]}>
                            <Icon name={'book'} size={25} />
                            <Text style={styles.subTitleTabButton}>Geral</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickTwo()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabTwoColor }]}>
                            <Icon name={'users'} size={25} />
                            <Text style={styles.subTitleTabButton}>Contatos</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickTree()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabTreeColor }]}>
                            <Icon5 name={'boxes'} size={25} />
                            <Text style={styles.subTitleTabButton}>Itens</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickFour()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabFourColor }]}>
                            <Icon5 name={'cart-arrow-down'} size={25} />
                            <Text style={styles.subTitleTabButton}>O.C's</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerTabBody}>
                        {
                            this.state.tabRender
                        }
                    </View>
                </Body>
                <Footer>

                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    foto: {
        width: heightWindow > 1000 ? 280 : 220,
        height: heightWindow > 1000 ? 280 : 220,
        resizeMode: 'center',
    },
    containerButtonsImage: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    tabView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        height: 55,
    },
    tabViewButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ADADAD",
        width: '25%',
        opacity: 0.5
    },
    subTitleTabButton: {
        fontSize: 12
    },
    containerBodyTabView: {
        flexDirection: 'row',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        width: '100%',
        height: '42%',
        backgroundColor: '#FFF'
    },

    scene: {
        flex: 1,
    },
    containerTabBody: {
        backgroundColor: '#FFF',
        flex: 1,
        flexDirection: 'row'
    }
})