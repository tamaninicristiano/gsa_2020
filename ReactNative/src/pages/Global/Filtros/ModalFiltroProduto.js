import React, { Component } from 'react'
import { Modal, View, StyleSheet, TouchableOpacity, TextInput, FlatList, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import Header from '../../../components/Template/Header'
import Body from '../../../components/Template/Body'
import Footer from '../../../components/Template/Footer'
import ImageBack from '../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../components/Estrutura/TitleScreen'
import ButtonOneFooter from '../../../components/Estrutura/ButtonOneFooter'
import ProdutoLista from '../../Suprimentos/components/ProdutoLista'

export default class ModalFiltroProduto extends Component {
    state = {
        CODIGOPRODUTO: null,
        produtos: [],
        filtroNome: null
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.loadProdutos()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    loadProdutos = async () => {
        try {
            const res = await axios.post(`${this.state.ip}:${this.state.porta}/filtroProdutoCompra`, {
                filtroNome: this.state.filtroNome
            })
            this.setState({ produtos: res.data })
        } catch (err) {
            showError(err)
        }
    }

    filtrar = async (item) => {
        await this.setState({ CODIGOPRODUTO: item.CODIGO })
        await this.props.onFiltrar(this.state.CODIGOPRODUTO)
        await this.setState({ CODIGOPRODUTO: null })
    }

    render() {
        return (
            <Modal style={styles.container} transparent={true} visible={this.props.isVisible}
                onRequestClose={this.props.onCancel}
                animationType='fade'>
                <View style={styles.containerFiltro}>
                    <Header />
                    <Body img={ImageBack}>
                        <TitleScreen title='Filtro Produto' />
                        <View style={styles.containerCampos}>
                            <View style={styles.input}>
                                <View style={styles.Icon}>
                                    <Icon name='archive' size={20} color='#000000' />
                                </View>
                                <View style={{ width: '80%', alignItems: 'center' }}>
                                    <TextInput placeholder='Nome do Produto'
                                        onChangeText={nome => this.setState({ filtroNome: nome })}
                                        onEndEditing={this.onPress} textAlign='center'
                                    />
                                </View>
                                <View style={styles.Icon}>
                                    <TouchableOpacity onPress={() => this.loadProdutos()} >
                                        <Icon name='filter' size={20} color='#000000' />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <FlatList data={this.state.produtos}
                            keyExtractor={item => `${item.CODIGO}`}
                            renderItem={({ item }) =>
                                <TouchableOpacity onPress={() => this.filtrar(item)}>
                                    <ProdutoLista codigo={item.CODIGO}
                                        produto={item.PRODUTO}
                                        prateleira={item.PRATELEIRA}
                                        image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`} />
                                </TouchableOpacity>}
                        />

                    </Body>
                    <Footer>
                        <ButtonOneFooter
                            onClickButtonCenter={this.props.onCancel}
                            IconPrincipal='close'
                            colorPrincipal='#8A0808'
                        />
                    </Footer>
                </View>
            </Modal >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1
    },
    containerFiltro: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    containerCampos: {
        justifyContent: "center",
        alignItems: 'center'
    },
    input: {
        height: 40,
        backgroundColor: '#EEE',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    Icon: {
        width: '10%', justifyContent: 'center', alignItems: 'center'
    },
})