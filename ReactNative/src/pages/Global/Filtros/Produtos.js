import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import Header from '../../../components/Template/Header'
import Body from '../../../components/Template/Body'
import Footer from '../../../components/Template/Footer'

import ImagemBack from '../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../components/Estrutura/TitleScreen'
import ButtonsTreeFooter from '../../../components/Estrutura/ButtonsTreeFooter'


export default class Produtos extends Component {
    state = {
        ip: '',
        porta: ''
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    onSair = async () => {
        await this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={styles.container}>
                <Header />
                <Body img={ImagemBack}>
                    <TitleScreen title="Filtro de Produtos" />

                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonLeft={this.Atualizar}
                        onClickButtonCenter={this.ShowFiltro}
                        onClickButtonRight={this.onSair}

                        IconLeft='refresh'
                        IconPrincipal='filter'
                        IconRight='close'

                        colorLeft='#2c3348'
                        colorPrincipal='#000000'
                        colorRight='#8A0808'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
