import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    TouchableOpacity,
    Image
} from 'react-native'
import backgroudImage from '../../assets/img/ImagemBackgroud.jpg'
import logoImage from '../../assets/img//Logo.png'
import AuthInput from '../../components/Estrutura/AuthInput'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class Auth extends Component {
    state = {
        ip: '192.168.0.241',
        porta: '3000',
        stageNew: false,
        nome: '',
        email: '',
        senha: '',
        confirmaSenha: '',
        cnpjcpf: ''
    }

    async componentDidMount() {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                // Alert.alert('Dados', dados.ip)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    signin = async () => {
        await this.conexaoDB()
        try {
            const res = await axios.post(`${this.state.ip}:${this.state.porta}/signin`, {
                email: this.state.email,
                senha: this.state.senha
            })

            const resUsu = await axios.get(`${this.state.ip}:${this.state.porta}/usuarioCorrente/${this.state.email}`)

            AsyncStorage.setItem('usuarioCorrente', JSON.stringify(resUsu.data))
            AsyncStorage.setItem('userData', JSON.stringify(res.data))
            axios.defaults.headers.common['Authorization'] = `bearer ${res.data.token}`
            this.props.navigation.navigate('Menu', res.data)
        } catch (err) {
            Alert.alert('Erro', 'Falha no login')
        }
    }

    signup = async () => {
        try {
            await axios.post(`${server}/usuario`, {
                nome: this.state.nome,
                email: this.state.email,
                senha: this.state.senha,
                confirmaSenha: this.state.confirmaSenha,
                cnpjcpf: this.state.cnpjcpf
            })

            Alert.alert('Sucesso', 'Usuário cadastrado!')
            this.setState({ stageNew: false })
        } catch (err) {
            showError(err)
        }
    }

    signinOrSignup = async () => {
        if (this.state.stageNew) {
            this.signup()
        } else {
            this.signin()
        }
    }

    dadosConexao = () => {
        this.props.navigation.navigate('ConfiguracoesConexao');
    }

    render() {
        const validacoes = []

        validacoes.push(this.state.email && this.state.email.includes('@'))
        validacoes.push(this.state.senha && this.state.senha.length >= 6)

        const validForm = validacoes.reduce((all, v) => all && v)

        return (
            <ImageBackground source={backgroudImage}
                style={styles.backgroud}>
                <Image style={styles.logo} source={logoImage} />
                <View style={styles.formContainer}>

                    <AuthInput icon="at" placeholder='E-mail' style={styles.input}
                        onChangeText={email => this.setState({ email })} />
                    <AuthInput keyboardType='numeric' icon="unlock" secureTextEntry={true} placeholder='Senha' style={styles.input}
                        onChangeText={senha => this.setState({ senha })} />

                    <TouchableOpacity disabled={!validForm} onPress={this.signinOrSignup}>
                        <View style={[styles.button, !validForm ? { backgroundColor: '#AAA' } : {}]}>
                            <Text style={styles.buttonText}>Entrar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.addBotao}
                    activeOpacity={0.7}
                    onPress={() => this.dadosConexao()}>
                    <Icon name="gears" size={20} color="#FFF" />
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroud: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: "center",
    },
    title: {
        fontSize: 70,
        marginBottom: 10,
    },
    formContainer: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 20,
        width: '90%',
    },
    input: {
        marginTop: 10,
        backgroundColor: '#FFF',
    },
    button: {
        backgroundColor: '#2c3348',
        marginTop: 10,
        padding: 10,
        alignItems: 'center',
    },
    buttonText: {
        color: '#DCDCDC',
        fontSize: 20
    },
    buttonTextConta: {
        color: '#000000',
        fontSize: 20
    },
    logo: {
        height: 35,
        width: 300,
        padding: 30,
        marginBottom: 15,
        justifyContent: 'center',
    },
    addBotao: {
        position: 'absolute',
        right: 30,
        bottom: 30,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '#2c3348',
        justifyContent: 'center',
        alignItems: 'center'
    }
})