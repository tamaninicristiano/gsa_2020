import React, { Component } from 'react'
import {
    View,
    ActivityIndicator,
    StyleSheet,
    ImageBackground,
    Image,
    Text,
    TouchableOpacity,
    Alert
} from 'react-native'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'
import logo from '../../assets/img/LogoHeader.png'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class AuthOrApp extends Component {

    componentDidMount = async () => {
        await this.logar()
    }

    logar = async () => {
        const userDataJson = await AsyncStorage.getItem('userData')
        let userData = null
        try {
            userData = JSON.parse(userDataJson)
        } catch (e) {
            Alert.alert('Erro', e)
        }

        if (userData && userData.token) {
            axios.defaults.headers.common['Authorization'] = `bearer ${userData.token}`
            this.props.navigation.navigate('Menu', userData)
        } else {
            this.props.navigation.navigate('Auth')
        }
    }

    render() {
        return (
            <ImageBackground source={ImagemBack} style={styles.container}>
                <TouchableOpacity onPress={() => this.logar()}>
                    <View style={styles.containerLogoCentral}>
                        <Image source={logo} style={styles.logo} />
                    </View>
                </TouchableOpacity>

                <ActivityIndicator size='large' />
                <Text>Carregando. Aguarde...</Text>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000'
    },
    containerLogoCentral: {
        backgroundColor: '#AAA',
        opacity: 0.8,
        height: 55,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    logo: {
        height: 35,
        width: 185,
        padding: 5
    },
})