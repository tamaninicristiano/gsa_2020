import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'
import Body from '../../components/Template/Body'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'
import logo from '../../assets/img/LogoHeader.png'

//onPress={() => this.props.navigation.openDrawer()}
export default class Menu extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name='bars' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                   <Text>Módulo Financeiro</Text>
                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}>Footer</Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
   
    
})
