import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../../components/Template/Header'
import Footer from '../../../components/Template/Footer'
import Body from '../../../components/Template/Body'

import ImagemBack from '../../../assets/img/ImagemBackgroud.jpg'
import ExpedicaoLista from '../components/ExpedicaoLista'

//onPress={() => this.props.navigation.openDrawer()}
export default class Menu extends Component {
    state = {
        expedicoes: [{
            ID: 1,
            DATA: "02/03/2020",
            RETIRADOPOR: "Iago",
            APELIDO: "MUNDIAL LOCACAO",
            MUNICIPIO: "Blumenau",
            CORICONE: "#DF7401",
            CORFUNDO: "#E6E6E6"
        },
        {
            ID: 2,
            DATA: "02/03/2020",
            RETIRADOPOR: "Taylor",
            APELIDO: "SUPREMO CIMENTOS",
            MUNICIPIO: "Pomerode",
            CORICONE: "#0B610B",
            CORFUNDO: "#E6E6E6"
        },
        {
            ID: 3,
            DATA: "03/03/2020",
            RETIRADOPOR: "Ivando",
            APELIDO: "CIA DE CIMENTO ITAMBÉ",
            MUNICIPIO: "Blumenau",
            CORICONE: "#6E6E6E",
            CORFUNDO: "#E6E6E6"
        },
        {
            ID: 4,
            DATA: "05/03/2020",
            RETIRADOPOR: "Marcelo",
            APELIDO: "MAX MOHR",
            MUNICIPIO: "Blumenau",
            CORICONE: "#0B610B",
            CORFUNDO: "#E6E6E6"
        },
        {
            ID: 5,
            DATA: "10/03/2020",
            RETIRADOPOR: "Cristiano",
            APELIDO: "CASA DAS CORRENTES",
            MUNICIPIO: "Porto Alegre",
            CORICONE: "#0B0B3B",
            CORFUNDO: "#E6E6E6"
        },
        ]
    }

    voltar = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.voltar()}>
                        <Icon name='arrow-left' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                    <ScrollView>
                        {this.state.expedicoes.map((item, i) => {
                            return (<View key={i}>
                                <TouchableOpacity>
                                    <ExpedicaoLista numero={item.ID} data={item.DATA} pessoa={item.APELIDO} municipio={item.MUNICIPIO} situacaoCor={item.CORICONE} colorBack={item.CORFUNDO} />
                                </TouchableOpacity>
                            </View>
                            )
                        })}
                    </ScrollView>
                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}>Footer</Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },


})
