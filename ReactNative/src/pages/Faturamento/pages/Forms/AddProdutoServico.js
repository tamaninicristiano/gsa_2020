import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableOpacity, Image, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'
import ImageBack from '../../../../assets/img/ImagemBackgroud.jpg'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ImageCard from '../../../../components/Estrutura/ImageCard'
import QRCode from '../../../Global/components/qrCode'
import ModalFiltroProduto from '../../../Global/Filtros/ModalFiltroProduto'

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

export default class AddProdutoServico extends Component {
    state = {
        ip: '',
        porta: '',

        placeHolderCodigo: 'Código do Produto:',
        placeHolderQuantidade: 'Quantidade:',
        codigoFiltro: null,
        showQRCodeModal: false,
        showFiltroModal: false,

        ID: null,
        PRODUTO_ID: null,
        CODIGO: null,
        PRODUTO: null,
        QUANTIDADE: 1,
        ORDERMSERVICOITEM: null
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    onPress = async () => {
        if (this.state.codigoFiltro != '') {
            try {
                const resP = await axios.get(`${this.state.ip}:${this.state.porta}/produto/${this.state.codigoFiltro}`)
                this.setState({
                    PRODUTO_ID: resP.data[0].ID,
                    CODIGO: resP.data[0].CODIGO,
                    PRODUTO: resP.data[0].PRODUTO
                })

            } catch (err) {

            }
        }
    }

    onSave = async () => {
        if (this.state.CODIGO != '999999' & this.state.CODIGO != '' & this.state.CODIGO != null) {
            if (this.state.QUANTIDADE) {
                try {
                    const params = await this.props.navigation.state.params;
                    //Alert.alert('Parametros', `${params}`)
                    await axios.post(`${this.state.ip}:${this.state.porta}/ordemServicoManutencaoItem`, {
                        ORDEMSERVICOITEM: params,
                        ITEM: this.state.PRODUTO_ID,
                        QUANTIDADE: this.state.QUANTIDADE
                    })
                    await this.Sair()
                } catch (err) {
                    Alert.alert('Erro', `${err}`)
                }
            } else {
                Alert.alert('Atenção!', 'Informe a quantidade!')
            }
        } else {
            Alert.alert('Atenção!', 'Produto não existe!')
        }
    }

    Sair = async () => {
        await this.props.navigation.goBack()
    }

    ShowQRCode = async () => {
        await this.setState({ showQRCodeModal: true })
    }

    ShowFiltro = async () => {
        await this.setState({ showFiltroModal: true })
    }

    FiltroProduto = async FiltroProduto => {
        //await Alert.alert('Dados', `${FiltroProduto}`)
        await this.setState({ showQRCodeModal: false })
        await this.setState({ showFiltroModal: false })
        await this.setState({ codigoFiltro: FiltroProduto })
        this.setState({ placeHolderCodigo: `${FiltroProduto}` })
        await this.onPress()
    }

    render() {
        return (
            <View style={styles.container}>
                <QRCode isVisible={this.state.showQRCodeModal}
                    onCancel={() => this.setState({ showQRCodeModal: false })}
                    onFiltrar={this.FiltroProduto}
                />
                <ModalFiltroProduto
                    isVisible={this.state.showFiltroModal}
                    onCancel={() => this.setState({ showFiltroModal: false })}
                    onFiltrar={this.FiltroProduto}
                />
                <Header />
                <Body img={ImageBack}>
                    <TitleScreen title='Adicionar Produto' />
                    <View style={styles.containerForm}>
                        <View style={styles.input}>
                            <View style={styles.Icon}>
                                <Icon name='archive' size={20} color='#000000' />
                            </View>
                            <View style={{ width: '80%', alignItems: 'center' }}>
                                <TextInput keyboardType='numeric' placeholder={this.state.placeHolderCodigo}
                                    onChangeText={codigo => this.setState({ codigoFiltro: codigo })}
                                    onEndEditing={this.onPress} textAlign='center'
                                />
                            </View>
                            <View style={styles.Icon}>
                                <TouchableOpacity onPress={() => this.setState({ showFiltroModal: true })}>
                                    <Icon name='search' size={20} color='#000000' />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.input}>
                            <View style={styles.Icon}>
                                <Icon name='check-square-o' size={20} color='#000000' style={{ marginLeft: 15, width: 25 }} />
                            </View>
                            <View style={{ width: '80%', alignItems: "center" }}>
                                <TextInput keyboardType='numeric' placeholder={this.state.placeHolderQuantidade} textAlign='center'
                                    onChangeText={quantidade => this.setState({ QUANTIDADE: quantidade })}
                                    name='txtQuantidade'
                                />
                            </View>
                            <View style={{ width: '10%', }} />
                        </View>

                    </View>
                    <ImageCard style={styles.Card}>
                        <View style={styles.containerCard}>
                            <View style={styles.containerCardHeader}>
                                <Text style={{ fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                                    {this.state.CODIGO} -  {this.state.PRODUTO}
                                </Text>
                            </View>
                            <View style={styles.containerCardBody}>
                                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/image/${this.state.CODIGO}.png` }} style={styles.foto} />
                            </View>
                        </View>
                    </ImageCard>
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonRight={this.onSave}
                        onClickButtonCenter={this.ShowQRCode}
                        onClickButtonLeft={this.Sair}

                        IconLeft='close'
                        IconPrincipal='qrcode'
                        IconRight='save'

                        colorLeft='#8A0808'
                        colorPrincipal='#000000'
                        colorRight='#298A08'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    containerForm: {
        justifyContent: "center",
        alignItems: 'center'
    },
    Icon: {
        width: '10%', justifyContent: 'center', alignItems: 'center'
    },
    input: {
        height: 50,
        backgroundColor: '#EEE',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        marginBottom: 15
    },
    Card: {
        marginLeft: 15,
        marginRight: 15,
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardHeader: {
        width: '90%',
        height: '15%',
        alignItems: 'center',
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    foto: {
        height: 220,
        width: 220
    },
    InfAdicional: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 80
    },
    fotoPessoa: {
        height: 150,
        width: 150
    },
    imagePessoa: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        height: 55,
        width: 55
    },
    containerCard: {
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
