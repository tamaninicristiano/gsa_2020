import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Dimensions, Alert, TouchableOpacity } from 'react-native'
import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'

import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ImageBack from '../../../../assets/img/ImagemBackgroud.jpg'

import Icon from 'react-native-vector-icons/FontAwesome'
import Icon5 from 'react-native-vector-icons/FontAwesome5'

const heightWindow = Dimensions.get('window').height

import TabGeral from '../../components/TabGeralVeiculos'
import TabItens from '../../components/TabItens'
import TabImagens from '../../components/TabImagens'

import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

export default class OrdemServicoManutencao extends Component {
    state = {
        ip: '',
        porta: '',

        tabSelect: 1,
        tabRender: null,
        tabOneColor: '#ADADAD',
        tabTwoColor: '',
        tabTreeColor: '',
        tabFourColor: '',
        itens: [],
        imagens: [],

        ID: null,
        OSNUMERO: null,
        OSDATA: null,
        PESSOAID: null,
        PESSOACODIGO: null,
        PESSOA: null,
        CNPJCPF: null,
        SITUACAO: null,
        SEQUENCIAAPROVACAO: null,
        PROCODIGO: null,
        PRODUTO: null,
        QUANTIDADEOS: null,
        CORFONTESITUACAO: '#000000',
        DESCRICAOPROBLEMA: null,
        SOLUCAO: null,
        PLACA: null,
        MODELOBOMBA: null

    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.tabActive()
        await this.carregamento()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregamento = async () => {
        try {
            const params = await this.props.navigation.state.params;

            await this.setState({
                ID: params.ID,
                OSNUMERO: params.NUMERO,
                OSDATA: params.DATA,
                PESSOAID: params.PESSOA_ID,
                PESSOACODIGO: params.PESSOA_CODIGO,
                PESSOA: params.PESSOA,
                CNPJCPF: params.CNPJCPF,
                SITUACAO: params.SITUACAO,
                SEQUENCIAAPROVACAO: params.SEQUENCIAAPROVACAO,
                CORFONTESITUACAO: params.CORFONTESITUACAO,
                DESCRICAOPROBLEMA: params.DESCRICAOPROBLEMA,
                SOLUCAO: params.SOLUCAO,
                PLACA: params.PLACA,
                MODELOBOMBA: params.MODELOBOMBA
            })

            await this.carregamentoItens()

            const imgs = await axios.get(`${this.state.ip}:${this.state.porta}/ordemServicoManutencaoItemImagens/${params.ID}`)
            await this.setState({
                imagens: imgs.data
            })

            await this.tabActive()
        } catch (err) {
            Alert.alert('Atenção', `${err}`)
        }
    }

    carregamentoItens = async () => {
        //Alert.alert('Passou aqui')
        try {
            if (this.state.SITUACAO == "Concluída" || this.state.SITUACAO == 'Aprovado' || this.state.SITUACAO == 'Em Aprovação' && this.state.SEQUENCIAAPROVACAO == 'Orçamento'
                || this.state.SITUACAO == 'Em Aprovação' && this.state.SEQUENCIAAPROVACAO == 'Aprovação do Cliente.') {
                const resItens = await axios.get(`${this.state.ip}:${this.state.porta}/ordemServicoVeiculosItensERP/${this.state.ID}`)
                //Alert.alert('Passou aqui')
                await this.setState({
                    itens: resItens.data
                })
            } else {
                const resItens = await axios.get(`${this.state.ip}:${this.state.porta}/ordemServicoVeiculosItens/${this.state.ID}`)
                await this.setState({
                    itens: resItens.data
                })
            }
            await this.tabActive()
        } catch (err) {
            Alert.alert('Atenção', `${err}`)
        }
    }

    tabActive = () => {
        const tabOne = <TabGeral
            cnpjcpf={this.state.CNPJCPF}
            cliente={this.state.PESSOA}
            situacao={this.state.SITUACAO}
            seqAprovacao={this.state.SEQUENCIAAPROVACAO}
            colorFont={this.state.CORFONTESITUACAO}
            descricaoProblema={this.state.DESCRICAOPROBLEMA}
            solucao={this.state.SOLUCAO}
            osNumero={this.state.OSNUMERO}
            osData={this.state.OSDATA}
            codigoPessoa={this.state.PESSOACODIGO}
            pessoa={this.state.PESSOA}
            placa={this.state.PLACA}
            modeloBomba={this.state.MODELOBOMBA}

            ip={this.state.ip}
            porta={this.state.porta}
        />

        const tabTwo = <TabItens
            situacao={this.state.SITUACAO}
            seqAprovacao={this.state.SEQUENCIAAPROVACAO}
            produtos={this.state.itens}
            atualizarItens={this.carregamentoItens}
        />


        const tabTree = <TabImagens
            ip={this.state.ip}
            porta={this.state.porta}
            imagens={this.state.imagens}
        />

        const tabFour = <TabGeral

        />

        if (this.state.tabSelect === 1) {
            return this.setState({ tabRender: tabOne })
        } else if (this.state.tabSelect === 2) {
            return this.setState({ tabRender: tabTwo })
        } else if (this.state.tabSelect === 3) {
            return this.setState({ tabRender: tabTree })
        } else if (this.state.tabSelect === 4) {
            return this.setState({ tabRender: tabFour })
        } else {
            this.setState({ tabRender: tabOne })
        }
    }

    clickOne = async () => {
        await this.setState({
            tabSelect: 1,
            tabOneColor: '#ADADAD',
            tabTwoColor: null,
            tabTreeColor: null,
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickTwo = async () => {
        await this.setState({
            tabSelect: 2,
            tabOneColor: null,
            tabTwoColor: '#ADADAD',
            tabTreeColor: null,
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickTree = async () => {
        await this.setState({
            tabSelect: 3,
            tabOneColor: null,
            tabTwoColor: null,
            tabTreeColor: '#ADADAD',
            tabFourColor: null
        })
        await this.tabActive()
    }

    clickFour = async () => {
        await this.setState({
            tabSelect: 4,
            tabOnelColor: null,
            tabTwoColor: null,
            tabTreeColor: null,
            tabFourColor: '#ADADAD'
        })
        await this.tabActive()
    }

    onClose = () => {
        this.props.navigation.goBack()
    }

    onClickPage = async (item) => {
        this.props.navigation.navigate('AddProdutoServicoVeiculo', this.state.ID);
    }

    addFoto = async (item) => {
        this.props.navigation.navigate('AddPhoto', this.state.ID)
    }

    render() {
        return (
            <View style={styles.container}>
                <Header />
                <Body img={ImageBack}>
                    <TitleScreen title='Cadastro O.S Manutenção' />
                    <TouchableOpacity style={styles.camera}>
                        <Icon name='camera' size={25} />
                    </TouchableOpacity>
                    <View style={styles.tabView}>
                        <TouchableOpacity
                            onPress={() => this.clickOne()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabOneColor }]}>
                            <Icon name={'book'} size={25} />
                            <Text style={styles.subTitleTabButton}>Geral</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickTwo()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabTwoColor }]}>
                            <Icon5 name={'boxes'} size={25} />
                            <Text style={styles.subTitleTabButton}>Itens</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickTree()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabTreeColor }]}>
                            <Icon name={'image'} size={25} />
                            <Text style={styles.subTitleTabButton}>Imagens</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.clickFour()}
                            style={[styles.tabViewButton, { backgroundColor: this.state.tabFourColor }]}>
                            <Icon5 name={'list-alt'} size={25} />
                            <Text style={styles.subTitleTabButton}>Procedimentos</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerTabBody}>
                        {
                            this.state.tabRender
                        }
                    </View>
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonLeft={this.onClose}
                        IconLeft='close'
                        colorLeft='#000000'

                        onClickButtonCenter={this.onClickPage}
                        IconPrincipal='archive'
                        colorPrincipal='#000000'

                        onClickButtonRight={this.carregamento}
                        IconRight='refresh'
                        colorRight='#000000'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        height: 55,
    },
    tabViewButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ADADAD",
        width: '25%',
        opacity: 0.5
    },
    subTitleTabButton: {
        fontSize: 12
    },
    containerBodyTabView: {
        flexDirection: 'row',
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        width: '100%',
        height: '42%',
        backgroundColor: '#FFF'
    },
    scene: {
        flex: 1,
    },
    containerTabBody: {
        backgroundColor: '#FFF',
        flex: 1,
        flexDirection: 'row'
    },
    camera: {
        position: 'absolute',
        alignSelf: 'flex-end',
        padding: 15
    }
})
