import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Alert } from 'react-native'

import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Header from '../../../components/Template/Header'
import Body from '../../../components/Template/Body'
import Footer from '../../../components/Template/Footer'

import ImageBack from '../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../components/Estrutura/TitleScreen'
import ButtonCard from '../../../components/Estrutura/ButtonCard'
import ButtonOneFooter from '../../../components/Estrutura/ButtonOneFooter'

export default class DashboardFaturamento extends Component {
    state = {
        ip: null,
        porta: null,
        valorFaturamento: '',
        qtdClientesAtivos: '',
        qtdClientesInativos: '',
        cotaG: '0,00'
    }

    componentDidMount = async () => {
        await this.conexaoDB();
        await this.carregar()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregar = async () => {
        try {
            const ativos = await axios.get(`${this.state.ip}:${this.state.porta}/clientesAtivos`)
            const inativos = await axios.get(`${this.state.ip}:${this.state.porta}/clientesInativos`)
            const CotaGeral = await axios.get(`${this.state.ip}:${this.state.porta}/cotaGeral`)
            this.setState({
                qtdClientesAtivos: ativos.data.length
            })
            this.setState({
                qtdClientesInativos: inativos.data.length
            })
            if (CotaGeral.data[0].COTAGERAL) {
                this.setState({
                    cotaG: CotaGeral.data[0].COTAGERAL
                })
            }



        } catch (err) {
            Alert.alert('Atenção!', 'Problemas com a conexão do Servidor!')
        }
    }

    OpenPage = (tela, param) => {
        this.props.navigation.navigate(tela, param);
    };

    render() {
        return (
            <View style={styles.container}>
                <Header />
                <Body img={ImageBack}>
                    <TitleScreen title="Dashboard Faturamento" />
                    <View style={styles.Body}>
                        <View style={styles.containerItem}>

                            <TouchableOpacity onPress={() => this.OpenPage('ListaCotas')} >
                                <ButtonCard title="Cotas" icone="dashboard" divPadding={20} div={1} style={styles.cardBig} styleIcon={{ color: '#130A52' }}
                                    infoCard={`${this.state.cotaG} %`} styleInfo={{ color: '#0B610B', fontWeight: 'bold', fontSize: 20 }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaAtivosInativos', 1)} >
                                <ButtonCard title="Clientes Ativos" icone="star" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }}
                                    infoCard={`${this.state.qtdClientesAtivos}`} styleInfo={{ color: '#0B610B', fontWeight: 'bold', fontSize: 20 }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaAtivosInativos', 2)}  >
                                <ButtonCard title="Clientes Inativos" icone="star-o" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }}
                                    infoCard={`${this.state.qtdClientesInativos}`} styleInfo={{ color: '#8A0808', fontWeight: 'bold', fontSize: 20 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </Body>
                <Footer>
                    <ButtonOneFooter
                        onClickButtonCenter={this.carregar}
                        IconPrincipal='refresh'
                        colorPrincipal='#000000'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Body: {
        alignItems: 'center',
    },
    containerItem: {
        padding: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
})
