import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Alert, FlatList, TextInput, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

const Width = Dimensions.get('window').width

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'
import ButtonsTreeFooter from '../../../../components/Estrutura/ButtonsTreeFooter'

import ImagemBack from '../../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'

import OrdemServicoVeiculo from '../../components/OrdemServicoVeiculo'

export default class OrdensServicoVeiculos extends Component {
    state = {
        ip: '',
        porta: '',
        title: '',
        OrdensManutencaoVeiculos: [],
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregamento()

    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregamento = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/ordensServicoVeiculos`)
            await this.setState({
                OrdensManutencaoVeiculos: res.data
            })
        } catch (err) {
            Alert.alert('Atenção', 'Erro ao carregar! Verifique os dados de conexão na opção Configurações no menu de navegação!')
        }
    }

    onClickPage = async (item) => {
        this.props.navigation.navigate('OrdemServicoVeiculos', item);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={`Lista Manutenções Bomba`} />
                    <FlatList data={this.state.OrdensManutencaoVeiculos}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => this.onClickPage(item)}>
                                <OrdemServicoVeiculo numero={item.OS_NUMERO}
                                    placa={item.PLACA}
                                    modeloBomba={item.MODELOBOMBA}
                                    image={`${this.state.ip}:${this.state.porta}/imagePessoas/${item.PESSOA_CODIGO}.png`}
                                    codigoCliente={item.PESSOA_CODIGO}
                                    cliente={item.PESSOA}
                                    cnpjcpf={item.CNPJCPF}
                                    numeroOS={item.NUMERO}
                                    dataOS={item.DATA}
                                    quantidade={item.QUANTIDADE}
                                    situacao={item.SITUACAO}
                                    sequenciaAprovacao={item.SEQUENCIAAPROVACAO}
                                    corSituacao={item.CORFONTESITUACAO}
                                />
                            </TouchableOpacity>
                        } />
                </Body>
                <Footer>
                    <ButtonsTreeFooter
                        onClickButtonLeft={this.carregamento}
                        onClickButtonCenter={this.addItem}
                        onClickButtonRight={this.OpenPageComponentesProduto}

                        IconLeft='refresh'
                        IconPrincipal='qrcode'
                        IconRight='shopping-cart'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    containerFiltros: {
        padding: 10,
        width: '100%'
    },
    input: {
        marginLeft: 20,
        width: Width * 0.60
    },
    campoFiltro: {
        height: 40,
        backgroundColor: '#EEE',
        borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#FFF'
    },
    icon: {
        color: '#333',
        marginLeft: 20,
        marginRight: 20
    },
})