import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, FlatList } from 'react-native'
import PostImage from '../components/PostImage'

export default class TabImagens extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Body}>
                    <FlatList data={this.props.imagens}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <PostImage image={`${this.props.ip}:${this.props.porta}/imagesOS/${item.IDIMAGE}.png`} comentario={item.COMENTARIO} />
                        } />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Body: {
        flex: 1,
        marginVertical: 7,
        marginHorizontal: 7,
        flexDirection: 'row',
        marginVertical: 5,
        justifyContent: 'space-between'
    }
})
