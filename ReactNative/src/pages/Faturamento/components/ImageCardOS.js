import React from 'react'
import { View, StyleSheet, Image } from 'react-native'

export default props => (
    <View style={[styles.container]}>
        <View style={styles.containerImage}>
            <Image source={{ uri: props.image }} style={styles.foto} />
        </View>
    </View>
)

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        borderRadius: 7,
        marginBottom: 15,
        height: 85,
        width: 85,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#ADADAD",
        justifyContent: 'center'
    },
    containerImage: {
        flex: 1,
        margin: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    foto: {
        height: 75,
        width: 75
    }
})