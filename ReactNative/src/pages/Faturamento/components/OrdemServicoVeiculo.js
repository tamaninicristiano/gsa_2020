import React from 'react'
import { Text, View, StyleSheet, Image, Dimensions } from 'react-native'

const Height = Dimensions.get('window').height
const Width = Dimensions.get('window').width

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={styles.containerImage}>
                <Image source={{ uri: props.image }} style={styles.foto} />
            </View>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.placa} - {props.modeloBomba}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Cliente:  {props.codigoCliente} - {props.cliente}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>CNPJ/CPF: {props.cnpjcpf}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Nº OS: {props.numeroOS}</Text>
                    <Text style={styles.Subtitle}>Data: {props.dataOS}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={[styles.Subtitle]}>Situação: </Text>
                    <Text style={[styles.Subtitle, { color: props.corSituacao, fontWeight: 'bold' }]}>{props.situacao}</Text>
                    <View />
                </View>
                {props.sequenciaAprovacao ?
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Seq. Aprovação:</Text>
                        <Text style={[styles.Subtitle, {
                            fontSize: 17,
                            fontWeight: 'bold',
                            color: `${props.corSituacao}`
                        }]}> {props.sequenciaAprovacao}</Text>
                        <View />
                    </View>
                    : <View />}
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        minHeight: 55,
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: Width * 0.8
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: Width * 0.2
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: Height < 641 ? 12 : 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    }

})