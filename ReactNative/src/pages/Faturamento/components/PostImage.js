import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Dimensions } from 'react-native'
import AuthorImage from './AuthorImage'

export default class PostImage extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={{ uri: this.props.image }} style={styles.image} />
                <AuthorImage idImage={15} nomeUsuario='Cristiano Tamanini' />
                <View style={styles.containerComentario}>
                    <Text style={{ fontWeight: 'bold' }}>Comentário:   </Text>
                    <Text>{this.props.comentario}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderBottomWidth: 1
    },
    image: {
        backgroundColor: 'rgba(0,0,0,0.1)',
        width: '100%',
        height: Dimensions.get('window').width * 3 / 4,
        resizeMode: 'contain'
    },
    containerComentario: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 'auto',
        width: '95%',
        marginHorizontal: 10,
        marginBottom: 15
    }
})
