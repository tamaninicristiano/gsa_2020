import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Swipeable from 'react-native-gesture-handler/Swipeable'

export default props => {
    const getRightContent = () => {
        return (
            <TouchableOpacity style={styles.right}>
                <Icon name='trash' size={20} cor='#FFF' />
            </TouchableOpacity>
        )
    }

    return (
        <Swipeable renderRightActions={getRightContent}>
            <View style={[styles.container]}>
                <TouchableOpacity style={{ paddingLeft: 15 }}>
                    <Icon name='archive' size={20} />
                </TouchableOpacity>
                <View style={{ flex: 1 }}>
                    <View style={styles.ContainerTitle}>
                        <Text style={styles.title}>{props.pessoa} - {props.municipio}</Text>
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text>Nº: {props.numero}</Text>
                        <Text>Data: {props.data}</Text>
                    </View>
                </View>
                <View style={styles.containerIconSituacao}>
                    <Icon name={'circle'} size={20} color={props.situacaoCor} />
                </View>
            </View>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6"
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingLeft: 10,
    },
    title: {
        paddingLeft: 15,
        fontSize: 15,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 25,
        paddingRight: 10,
    },
    right: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        backgroundColor: '#8B0000'
    },
    containerIconSituacao: {
        padding: 15
    }
})