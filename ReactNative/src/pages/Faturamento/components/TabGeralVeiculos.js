import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Dimensions, Image } from 'react-native'
import ImageCard from '../../../components/Estrutura/ImageCard'

const widthWindow = Dimensions.get("window").width
const heightWindow = Dimensions.get("window").height

export default class TabOne extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageCard style={{ marginTop: 5, marginBottom: 5 }}>
                    <View style={styles.containerCard}>
                        <View style={styles.containerCardBody}>
                            <Image source={{ uri: `${this.props.ip}:${this.props.porta}/imagePessoas/${this.props.codigoPessoa}.png` }} style={styles.foto} />
                        </View>
                    </View>
                </ImageCard>
                <ScrollView>
                    <View style={styles.containerFilho}>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>CNPJ/CPF:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { color: '#000000', fontSize: 18, fontWeight: 'bold' }]}>{this.props.cnpjcpf}</Text>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Cliente:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps]}>{this.props.cliente}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: (widthWindow / 2) - 15 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Número OS:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.osNumero}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: (widthWindow / 2) - 15 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Data:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.osData}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: (widthWindow / 3) - 15 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Placa:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.placa}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: (widthWindow / 1) - 152 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Modelo Bomba:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={styles.labelProps}>{this.props.modeloBomba}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Problemas:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { marginHorizontal: 10 }]}>{this.props.descricaoProblema}</Text>
                            </View>
                        </View>
                        <View style={styles.inputText}>
                            <View style={styles.containerLabelIcon}>
                                <Text style={styles.label}>Soluções:</Text>
                            </View>
                            <View style={styles.containerLabelValor}>
                                <Text style={[styles.labelProps, { marginHorizontal: 10 }]}>{this.props.solucao}</Text>
                            </View>
                        </View>
                        {this.props.situacao == 'Em Aprovação' ? <View style={{ flexDirection: 'row', width: widthWindow, alignItems: 'center' }}>
                            <View style={[styles.inputText, { width: (widthWindow / 2) - 15 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Situação:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={[styles.labelProps, { color: this.props.colorFont, fontWeight: 'bold' }]}>{this.props.situacao}</Text>
                                </View>
                            </View>
                            <View style={[styles.inputText, { marginLeft: 6, width: (widthWindow / 2) - 15 }]}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Seq. Aprovação:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={[styles.labelProps, { color: this.props.colorFont, fontWeight: 'bold' }]}>{this.props.seqAprovacao}</Text>
                                </View>
                            </View>
                        </View> :
                            <View style={styles.inputText}>
                                <View style={styles.containerLabelIcon}>
                                    <Text style={styles.label}>Situação:</Text>
                                </View>
                                <View style={styles.containerLabelValor}>
                                    <Text style={[styles.labelProps, { color: this.props.colorFont, fontWeight: 'bold' }]}>{this.props.situacao}</Text>
                                </View>
                            </View>
                        }
                    </View>
                </ScrollView>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#AAA',
        alignContent: 'center',
        paddingHorizontal: 5,
        width: '90%'
    },
    containerFilho: {
        flex: 1,
        width: '100%',
        padding: 7
    },
    inputText: {
        marginTop: 10,
        height: 'auto',
        minHeight: 50,
        backgroundColor: '#FFF',
        borderRadius: 12,
    },
    containerLabelIcon: {
        paddingLeft: 15,
        flexDirection: 'row',
    },
    containerLabelValor: {
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold', fontSize: 15
    },
    labelProps: {
        fontSize: 15.5
    },
    containerCard: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerCardBody: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    },
    foto: {
        width: heightWindow > 1000 ? 280 : 220,
        height: heightWindow > 1000 ? 280 : 220,
        resizeMode: 'center'
    },
})
