import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {


    const getRigthContent = () => {
        return (
            <TouchableOpacity style={styles.Right}
                onPress={() => props.onDelete && props.onDelete('Deseja realamente cancelar está solicitação?', props.id, props.situacao, props.seqAprovacao)}>
                <Icon name="trash-o" size={30} color="#FFF" />
            </TouchableOpacity>
        )
    }

    return (
        <Swipeable
            renderRightActions={getRigthContent}>
            <View style={[styles.container]}>
                <View style={styles.containerImage}>
                    <Image source={{ uri: props.image }} style={styles.foto} />
                </View>
                <View style={styles.containerInfo}>
                    <View style={styles.ContainerTitle}>
                        <Text style={styles.title}>{props.produto}</Text>
                    </View>
                    <View style={styles.ContainerSubtitle}>
                        <Text style={styles.Subtitle}>Código: {props.codigo}</Text>
                        {props.isVisiblePrateleira ?
                            <Text style={styles.Subtitle}>Prateleira: {props.prateleira}</Text>
                            : <Text style={styles.Subtitle}>Quantidade: {props.quantidade}</Text>
                        }
                        <Text />
                    </View>
                </View>
            </View>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        opacity: 0.75,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        minHeight: 70
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '80%'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%'
    },
    title: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 17,
        fontWeight: 'bold'
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    },
    Right: {
        backgroundColor: '#610B0B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 25
    }

})