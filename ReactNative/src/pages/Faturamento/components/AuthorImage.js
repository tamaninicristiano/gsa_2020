import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

export default class AuthorImage extends Component {
    state = {
        ip: '',
        porta: '',
        idImage: null
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={{ uri: `${this.state.ip}:${this.state.porta}/imagePerfis/${this.props.idImage}.png` }}
                    style={[styles.profile]} />
                <Text style={styles.nomeUsuario}>{this.props.nomeUsuario}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profile: {
        width: 30,
        height: 30,
        borderRadius: 15,
        marginHorizontal: 15
    },
    nomeUsuario: {
        color: '#444',
        marginVertical: 10,
        fontSize: 15,
        fontWeight: 'bold'
    }
})
