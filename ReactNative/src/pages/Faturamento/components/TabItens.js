import React, { Component } from 'react'
import { StyleSheet, View, FlatList, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Produto from './Produto'

export default class TabItensFornecedor extends Component {
    state = {
        ip: '',
        porta: '',
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    PerguntaDelete = async (msg, ID, SITUACAO, SEQAPROVACAO) => {
        Alert.alert(
            'Atenção!',
            msg,
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                { text: 'Confirmar', onPress: () => this.deleteItem(ID, SITUACAO, SEQAPROVACAO) },
            ],
            { cancelable: false },
        );
    }

    deleteItem = async (ID, SITUACAO, SEQAPROVACAO) => {
        // await Alert.alert('Parametros', `${this.state.porta}`)
        if (SITUACAO == 'Em Aprovação' && SEQAPROVACAO == 'Execução de Serviço') {
            try {
                await axios.delete(`${this.state.ip}:${this.state.porta}/ordemServicoVeiculoItem/${ID}`)
                await this.atualizarItens()
            } catch (err) {
                Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
            }
        } else {
            Alert.alert('Atenção', 'Somente é possível excluir na situação de Em aprovação na Sequência de Execução de Serviço!')
        }
    }

    atualizarItens = async () => this.props.atualizarItens()

    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.produtos}
                    keyExtractor={item => `${item.ID_ERP}`}
                    renderItem={({ item }) =>
                        <Produto
                            id={item.ID_ERP}
                            situacao={this.props.situacao}
                            seqAprovacao={this.props.seqAprovacao}
                            codigo={item.CODIGO}
                            produto={item.PRODUTO}
                            image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`}
                            quantidade={item.QUANTIDADE}
                            onDelete={this.PerguntaDelete}
                        />} />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
