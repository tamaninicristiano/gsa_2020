import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'
import Body from '../../components/Template/Body'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import ButtonCard from '../../components/Estrutura/ButtonCard'
import TitleScreen from '../../components/Estrutura/TitleScreen'
import ButtonOneFooter from '../../components/Estrutura/ButtonOneFooter'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'

export default class Menu extends Component {
    state = {
        valorFaturamento: 'R$ 0,00'
    }

    componentDidMount = async () => {
        await this.conexaoDB();
        await this.carregarValor();
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregarValor = async () => {
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/valorFaturamento`)
            this.setState({
                valorFaturamento: res.data[0].VALORLIQUIDO
            })
        } catch (err) {
            Alert.alert('Atenção', 'Problema com a conexão ao servidor!')
        }
    }

    Faturamento() {
        Alert.alert('Faturamento', 'Faturamento em desenvolvimento!')
    }

    SeparacaoPedido() {
        Alert.alert('Pedido', 'Separação de Pedido em Desenvolvimento')
    }

    OpenPage = (tela) => {
        this.props.navigation.navigate(tela);
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name='bars' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title="Módulo Faturamento" />
                    <View style={styles.Body}>
                        <View style={styles.containerItem}>
                            <TouchableOpacity onPress={() => this.OpenPage('DashboardFaturamento')} >
                                <ButtonCard title="DashBoard" icone="dashboard" divPadding={20} div={1} style={styles.cardBig} styleIcon={{ color: '#130A52' }}
                                    infoCard={`${this.state.valorFaturamento}`} styleInfo={{ color: '#0B610B', fontWeight: 'bold', fontSize: 20 }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('Expedicoes')} >
                                <ButtonCard title="Expedição" icone="truck" divPadding={30} div={2} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.SeparacaoPedido}>
                                <ButtonCard title="Separação de Pedidos" icone="shopping-cart" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaOrdensServicoManutencao')} >
                                <ButtonCard title="O.S. Manutenção" icone="wrench" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaOrdensServicoVeiculos')} >
                                <ButtonCard title="O.S. Bombas" icone="bus" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.Faturamento} >
                                <ButtonCard title="Faturamento" icone="trophy" divPadding={30} div={2} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </Body>
                <Footer>
                    <ButtonOneFooter
                        onClickButtonCenter={this.carregarValor}
                        IconPrincipal='refresh'
                        colorPrincipal='#000000'
                    />
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    Body: {
        alignItems: 'center',
    },
    containerItem: {
        padding: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    cardBig: {
        backgroundColor: '#FFF',

    }
})