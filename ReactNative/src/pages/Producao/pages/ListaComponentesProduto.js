import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Image, Text, Alert, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../../components/Template/Header'
import Body from '../../../components/Template/Body'
import Footer from '../../../components/Template/Footer'

import ImagemBack from '../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../components/Estrutura/TitleScreen'

import ComponenteProduto from '../components/ComponenteProduto'

export default class ListaComponentesProduto extends Component {
    state = {
        ip: '',
        porta: '',

        componentesProduto: []
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregamento()

    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregamento = async () => {
        try {
            const COD = await this.props.navigation.state.params;
            //await Alert.alert('Atenção', `${COD}`)
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/componentesProduto/${COD}`)
            await this.setState({
                componentesProduto: res.data
            })
        } catch (err) {
            Alert.alert('Atenção', 'Não foram encontradas componentes para o item selecionado.')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={'Listagem de Componentes'} />
                    <FlatList data={this.state.componentesProduto}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <ComponenteProduto
                                image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGO}.png`}
                                codigo={item.CODIGO}
                                componente={item.COMPONENTE}
                                unidade={item.UNIDADE}
                                quantidade={item.QUANTIDADE}
                                valorcusto={item.VALORCUSTO}
                                custototal={item.CUSTOTOTAL}
                            />
                        } />


                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}>Footer</Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
})