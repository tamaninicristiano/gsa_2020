import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={styles.containerImage}>
                <Image source={{ uri: props.image }} style={styles.foto} />
            </View>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.codigo} - {props.componente}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Unidade: {props.unidade}</Text>
                    <Text style={styles.Subtitle}>Quantidade: {props.quantidade}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>R$ Custo: {props.valorcusto}</Text>
                    <Text style={styles.Subtitle}>R$ Custo Total: {props.custototal}</Text>
                    <Text />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        opacity: 0.75,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        minHeight: 70,
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%'
    },
    containerInfo: {
        width: '80%'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%',
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    }

})