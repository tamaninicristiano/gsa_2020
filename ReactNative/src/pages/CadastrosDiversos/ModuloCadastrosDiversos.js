import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Header from '../../components/Template/Header'
import Footer from '../../components/Template/Footer'
import Body from '../../components/Template/Body'

import ButtonCard from '../../components/Estrutura/ButtonCard'
import TitleScreen from '../../components/Estrutura/TitleScreen'

import ImagemBack from '../../assets/img/ImagemBackgroud.jpg'

export default class ModuloCadastrosGerais extends Component {
    OpenPage = (tela, item) => {
        this.props.navigation.navigate(tela, item);
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                    <TouchableOpacity style={styles.ButtonMenu} onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name='bars' size={20} color="#D3D3D3" />
                    </TouchableOpacity>
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title="Módulo Cadastros Diversos" />
                    <View style={styles.Body}>
                        <View style={styles.containerItem}>
                            <TouchableOpacity onPress={() => this.OpenPage('ListaPessoas', { descricao: 'Clientes', filtro: 1 })}>
                                <ButtonCard title="Clientes" icone="user-circle-o" divPadding={30} div={1} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.OpenPage('ListaPessoas', { descricao: 'Fornecedores', filtro: 2 })}>
                                <ButtonCard title="Fornecedores" icone="users" divPadding={30} div={1} style={styles.cardBig} styleIcon={{ color: '#130A52' }} />
                            </TouchableOpacity>


                        </View>
                    </View>
                </Body>
                <Footer>
                    <View>
                        <Text style={{ color: '#FFF' }}></Text>
                    </View>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    Body: {
        alignItems: 'center',
    },
    containerItem: {
        padding: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    cardBig: {
        backgroundColor: '#FFF',
    }
})