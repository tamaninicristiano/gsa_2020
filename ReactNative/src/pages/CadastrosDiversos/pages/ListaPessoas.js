import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Alert, FlatList, TextInput, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import Icon from 'react-native-vector-icons/FontAwesome'

const Height = Dimensions.get('window').height
const Width = Dimensions.get('window').width

import Header from '../../../components/Template/Header'
import Body from '../../../components/Template/Body'
import Footer from '../../../components/Template/Footer'

import ImagemBack from '../../../assets/img/ImagemBackgroud.jpg'
import TitleScreen from '../../../components/Estrutura/TitleScreen'

import PessoaLista from '../components/PessoaLista'

export default class ListaReservaProduto extends Component {
    state = {
        ip: '',
        porta: '',
        title: '',
        tipofiltro: null,
        filtroNome: null,
        pessoas: [],

        dadosTela: {
            title: "Clientes"
        }
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregamento()

    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregamento = async () => {
        try {
            const params = await this.props.navigation.state.params;
            await this.setState({
                title: params.descricao,
                tipofiltro: params.filtro
            })
            //await Alert.alert('Atenção', `${params.filtro}`)
            const res = await axios.post(`${this.state.ip}:${this.state.porta}/pessoaTipo/${params.filtro}`)
            await this.setState({
                pessoas: res.data
            })
        } catch (err) {
            Alert.alert('Atenção', 'Erro ao carregar! Verifique os dados de conexão na opção Configurações no menu de navegação!')
        }
    }

    filtrar = async () => {
        if (this.state.filtroNome === null) {
            Alert.alert('Alerta', 'Para filtrar, informe um nome!')
        } else {
            try {
                const res = await axios.post(`${this.state.ip}:${this.state.porta}/pessoaTipo/${this.state.tipofiltro}`, {
                    filtroNome: this.state.filtroNome
                })
                await this.setState({
                    pessoas: res.data
                })
            } catch (err) {
                Alert.alert('Atenção', 'Erro ao carregar! Verifique os dados de conexão na opção Configurações no menu de navegação!')
            }
        }
    }

    onClickPage = async (item) => {
        this.props.navigation.navigate('FrmPessoa', item);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header} >
                    <Header />
                </View>
                <Body img={ImagemBack}>
                    <TitleScreen title={`Listagem de ${this.state.title} `} />
                    <View style={styles.containerFiltros}>
                        <View style={[styles.campoFiltro]}>
                            <Icon name='user' size={20} style={styles.icon} />
                            <TextInput placeholder='Digite uma parte do nome' style={styles.input} onChangeText={filtroNome => this.setState({ filtroNome })} />
                            <TouchableOpacity onPress={() => this.filtrar()}>
                                <Icon name='filter' size={20} style={styles.icon} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <FlatList data={this.state.pessoas}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => this.onClickPage(item)}>
                                <PessoaLista codigo={item.CODIGO}
                                    nome={item.NOME}
                                    cnpjcpf={item.CNPJCPF}
                                    image={`${this.state.ip}:${this.state.porta}/imagePessoas/${item.CODIGO}.png`}
                                    uf={item.UF}
                                    municipio={item.MUNICIPIO}
                                    ultimaVenda={item.UTLTIMAVENDA}
                                />
                            </TouchableOpacity>
                        } />
                </Body>
                <Footer>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Header: {
        backgroundColor: '#2c3348',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    },
    containerFiltros: {
        padding: 10,
        width: '100%'
    },
    input: {
        marginLeft: 20,
        width: Width * 0.60
    },
    campoFiltro: {
        height: 40,
        backgroundColor: '#EEE',
        borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#FFF'
    },
    icon: {
        color: '#333',
        marginLeft: 20,
        marginRight: 20
    },
})