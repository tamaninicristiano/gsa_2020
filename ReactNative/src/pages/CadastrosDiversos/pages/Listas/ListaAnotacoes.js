import React, { Component } from 'react'
import { Text, StyleSheet, View, Alert, TouchableOpacity, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import Header from '../../../../components/Template/Header'
import Body from '../../../../components/Template/Body'
import Footer from '../../../../components/Template/Footer'
import TitleScreen from '../../../../components/Estrutura/TitleScreen'
import ImageBack from '../../../../assets/img/ImagemBackgroud.jpg'

import Anotacao from '../../components/Anotacao'

export default class ListaAnotacoes extends Component {
    state = {
        ip: '',
        porta: '',

        anotacoes: []
    }
    componentDidMount = async () => {
        await this.conexaoDB()
        await this.carregarAnotacoes()
        //  
        // Alert.alert('Paramentro', `${params}`)
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    carregarAnotacoes = async () => {
        const params = await this.props.navigation.state.params;
        try {
            const res = await axios.get(`${this.state.ip}:${this.state.porta}/pessoaAnotacao/${params}`)
            this.setState({
                anotacoes: res.data
            })
        } catch (e) {
            Alert.alert('Erro', `${e}`)
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <Header />
                <Body img={ImageBack}>
                    <TitleScreen title={'Anotações'} />
                    <FlatList data={this.state.anotacoes}
                        keyExtractor={item => `${item.ID}`}
                        renderItem={({ item }) =>
                            <Anotacao numero={item.NUMERO}
                                data={item.DATA}
                                horaInicio={item.HORAINICIO}
                                horaFim={item.HORAFIM}
                                recurso={item.RECURSO}
                                observacoes={item.OBSERVACOES}
                            />
                        } />
                </Body>
                <Footer>

                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
