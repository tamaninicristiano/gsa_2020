import React, { Component } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

import DocumentoItensLista from './DocumentoItensLista'

export default class TabDocumentos extends Component {
    state = {
        ip: '',
        porta: ''
    }

    componentDidMount = async () => {
        await this.conexaoDB()
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.documentoItens}
                    keyExtractor={item => `${item.ID}`}
                    renderItem={({ item }) => <DocumentoItensLista
                        image={`${this.state.ip}:${this.state.porta}/image/${item.CODIGOSTUHLERT}.png`}
                        dataEmissao={item.DATAEMISSAO}
                        tipoDocumento={item.TIPODOCUMENTO}
                        produto={item.PRODUTO}
                        unidade={item.UNIDADE}
                        quantidade={item.QUANTIDADE}
                        codigoStuhlert={item.CODIGOSTUHLERT}
                        documento={item.DOCUMENTO}
                        valorUnitario={item.VALORUNITARIO}
                        valorliquido={item.VALORLIQUIDO} />}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})
