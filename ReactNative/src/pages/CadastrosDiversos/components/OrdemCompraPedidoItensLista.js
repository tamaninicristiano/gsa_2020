import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={[styles.containerImage]}>
                <Icon name="circle" size={20} color={props.colorSituacao} />
            </View>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.descricaoFornecedor}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Código:  {props.codigoStuhlert}</Text>
                    <Text style={styles.Subtitle}>OC./Ped.:  {props.ordemCompraPedido}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Med.:  {props.unidade}</Text>
                    <Text style={styles.Subtitle}>Qtd.:  {props.quantidade}</Text>
                    <Text style={styles.Subtitle}>R$ Total.:  {props.valorliquido}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Previsão Entrega:  {props.previsaoEntrega}</Text>
                    <Text />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '85%'
    },
    containerImage: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    }

})