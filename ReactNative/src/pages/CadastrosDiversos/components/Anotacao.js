import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>Número: {props.numero}</Text>
                    <Text>Data: {props.data}</Text>
                    <View />
                </View>
                <View style={styles.ContainerTitle}>
                    <Text>Hora Inicio: {props.horaInicio}</Text>
                    <Text>Hora Final: {props.horaFim}</Text>
                    <View />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Recurso: {props.recurso}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerObservacao}>
                    <Text style={styles.Subtitle}>Observações: {props.observacoes}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 5
    },
    containerInfo: {
        width: '90%',
        margin: 20
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    ContainerObservacao: {
        height: 'auto',
        width: '100%'
    }

})