import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default props => {
    return (
        <View style={[styles.container]}>
            <View style={[styles.containerImage]}>
                <Image source={{ uri: props.image }} style={styles.foto} />
            </View>
            <View style={styles.containerInfo}>
                <View style={styles.ContainerTitle}>
                    <Text style={styles.title}>{props.produto}</Text>
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Código:  {props.codigoStuhlert}</Text>
                    <Text style={styles.Subtitle}>Documento:  {props.documento}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Data Emissão:  {props.dataEmissao}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Med.:  {props.unidade}</Text>
                    <Text style={styles.Subtitle}>Qtd.:  {props.quantidade}</Text>
                    <Text style={styles.Subtitle}>R$ Total.:  {props.valorliquido}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Tipo Doc.: {props.tipoDocumento}</Text>
                    <Text />
                </View>
                <View style={styles.ContainerSubtitle}>
                    <Text style={styles.Subtitle}>Valor Unitário: {props.valorUnitario}</Text>
                    <Text />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#2E2E2E',
        borderBottomWidth: 1.2,
        paddingVertical: 7,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "#E6E6E6",
        height: 'auto',
        marginRight: 5
    },
    ContainerTitle: {
        flexDirection: 'row',
        paddingRight: 5
    },
    containerInfo: {
        width: '80%'
    },
    containerImage: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 15.5,
        fontWeight: 'bold'
    },
    ContainerSubtitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Subtitle: {
        paddingLeft: 8,
        fontSize: 15,
    },
    foto: {
        width: 55,
        height: 55,
        resizeMode: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(230, 230, 230,1)",
        borderColor: '#BDBDBD'
    },
})