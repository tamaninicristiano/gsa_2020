import React, { Component } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'

import OrdemCompraPedidoItens from './OrdemCompraPedidoItensLista'

export default class TabOrdensCompraPedidos extends Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.props.ordemCompraPedidoItens}
                    keyExtractor={item => `${item.ID}`}
                    renderItem={({ item }) => <OrdemCompraPedidoItens
                        unidade={item.UNIDADE}
                        quantidade={item.QUANTIDADE}
                        descricaoFornecedor={item.PRODUTO}
                        codigoStuhlert={item.CODIGOSTUHLERT}
                        ordemCompraPedido={item.ORDEMCOMPRAPEDIDO}
                        previsaoEntrega={item.PREVISAOENTREGA}
                        colorSituacao={item.SITUACAOCOR}
                        colorSituacaoBorder={item.SITUACAOCORBORDA}
                        valorliquido={item.VALORLIQUIDO} />}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})
