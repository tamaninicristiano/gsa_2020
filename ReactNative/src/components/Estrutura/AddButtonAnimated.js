import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableHighlight, TouchableOpacity, Animated } from 'react-native'
import Icon5 from 'react-native-vector-icons/FontAwesome5'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class AddButtonAnimated extends Component {
    buttonSize = new Animated.Value(1);
    mode = new Animated.Value(0)

    handlePress = () => {
        Animated.sequence([
            Animated.timing(this.buttonSize, {
                toValue: 0.90,
                duration: 10
            }),
            Animated.timing(this.buttonSize, {
                toValue: 1
            }),
            Animated.timing(this.mode, {
                toValue: this.mode._value === 0 ? 1 : 0
            })
        ]).start();
    }

    clickButtonRight = async () => {
        await this.props.onClickButtonRight()
        this.handlePress()
    }

    clickButtonCenter = async () => {
        await this.props.onClickButtonCenter()
        this.handlePress()
    }

    clickButtonLeft = async () => {
        await this.props.onClickButtonLeft()
        this.handlePress()
    }

    render() {
        const sizeStyle = {
            transform: [{ scale: this.buttonSize }]
        };

        const rotation = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "45deg"]
        })

        const Button1X = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [181, 120]
        })

        const Button1Y = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [-20, -45]
        })

        const Button2X = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [181, 181]
        })

        const Button2Y = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [-20, -80]
        })

        const Button3X = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [181, 244]
        })

        const Button3Y = this.mode.interpolate({
            inputRange: [0, 1],
            outputRange: [-20, -45]
        })

        return (
            <View style={styles.container}>
                <Animated.View style={{ position: 'absolute', left: Button1X, top: Button1Y }}>
                    <TouchableOpacity onPress={this.clickButtonLeft} underlayColor="#A4A4A4">
                        <View style={styles.secondaryButton}>
                            <Icon name={this.props.buttonLeft} size={24} color='#FFF' />
                        </View>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View style={[this.props.styleCenter, { position: 'absolute', left: Button2X, top: Button2Y }]}>
                    <TouchableOpacity onPress={this.clickButtonCenter} underlayColor="#A4A4A4">
                        <View style={styles.secondaryButton}>
                            <Icon name={this.props.buttonCenter} size={24} color='#FFF' />
                        </View>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View style={{ position: 'absolute', left: Button3X, top: Button3Y }}>
                    <TouchableOpacity onPress={this.clickButtonRight} underlayColor="#A4A4A4">
                        <View style={styles.secondaryButton}>
                            <Icon name={this.props.buttonRight} size={24} color='#FFF' />
                        </View>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View style={[styles.button, sizeStyle]}>
                    <TouchableOpacity onPress={this.handlePress}>
                        <Animated.View style={{ transform: [{ rotate: rotation }] }}>
                            <Icon5 name="plus" size={24} color="#FFF" />
                        </Animated.View>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#A4A4A4',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        borderRadius: 30,
        position: 'absolute',
        top: -30,
        shadowColor: '#7F58FF',
        shadowRadius: 5,
        shadowOffset: { height: 10 },
        shadowOpacity: 0.3,
        borderWidth: 2,
        borderColor: '#000000'
    },
    secondaryButton: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        width: 48,
        height: 48,
        borderRadius: 24,
        backgroundColor: '#2E2E2E',
    }
})
