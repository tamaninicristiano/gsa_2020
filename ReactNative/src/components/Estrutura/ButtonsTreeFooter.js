import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const widthWindow = Dimensions.get('window').width

export default class ButtonsTreeFooter extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={this.props.onClickButtonLeft}>
                        <View style={styles.button}>
                            <Icon name={this.props.IconLeft} size={20} color={this.props.colorLeft} />
                        </View>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={this.props.onClickButtonCenter}>
                        <View style={styles.buttonPrincipal}>
                            <Icon name={this.props.IconPrincipal} size={30} color={this.props.colorPrincipal} />
                        </View>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={this.props.onClickButtonRight}>
                        <View style={styles.button}>
                            <Icon name={this.props.IconRight} size={20} color={this.props.colorRight} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: widthWindow * 0.32,
        paddingRight: widthWindow * 0.32,
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center'
    },
    buttonPrincipal: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
