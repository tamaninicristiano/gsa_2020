import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, Alert } from 'react-native'
import axios from 'axios'

import ImagePicker from 'react-native-image-picker'

export default class AddFoto extends Component {
    state = {
        ip: 'http://192.168.0.16',
        porta: '3000',
        imagem: null
    }

    ImagePickerCallback = async (data) => {
        if (data.didCancel) {
            return;
        }
        if (data.error) {
            return;
        }
        if (!data.uri) {
            return;
        }

        this.setState({
            imagem: data
        })

    }

    uploadImagem = async () => {
        const data = new FormData()
        data.append('data', this.state.imagem)
        try {
            //Alert.alert('Dados', `${data}`)
            await axios.post(`${this.state.ip}:${this.state.porta}/postImagemOSVeiculos`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
        } catch (err) {
            Alert.alert('Erro', `${err}`)
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerImage}>
                    <Image style={styles.imagem} source={{
                        uri: this.state.imagem ? this.state.imagem.uri
                            : 'https://www.toinhabrasilshow.com/wp-content/uploads/2019/09/placeholder-300x200.png'
                    }} />
                </View>

                <TouchableOpacity style={styles.button} onPress={() => ImagePicker.showImagePicker({}, this.ImagePickerCallback)}>
                    <Text style={styles.buttonText}>Escolha uma Imagem</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => this.uploadImagem()}>
                    <Text style={styles.buttonText}>Enviar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        width: 150,
        height: 50,
        borderRadius: 3,
        backgroundColor: '#7159c1',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15
    },
    buttonText: {
        color: '#FFF',
    },
    containerImage: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 380,
        height: 385,
        borderWidth: 1,
        borderColor: '#000000',
        marginBottom: 10
    },
    imagem: {
        width: '95%',
        height: 380
    }
})
