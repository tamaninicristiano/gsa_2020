import React from 'react'
import { StyleSheet, View, TouchableOpacity, Text, } from 'react-native'

export default props => {
    return (
        <TouchableOpacity style={[styles.container, props.style]} onPress={props.click} disabled={props.habilitar}>
            <View>
                <Text style={styles.descricaoInput}>
                    {props.nome}
                </Text >
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '40%',
        height: 40,
        backgroundColor: '#2c3348',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    descricaoInput: {
        color: '#FFF'
    }
})