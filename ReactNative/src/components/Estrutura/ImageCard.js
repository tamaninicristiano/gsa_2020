import React from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'

const heightWindow = Dimensions.get('window').height

export default props => (
    <View style={[styles.container, props.style]}>
        {props.children}
    </View>
)

const styles = StyleSheet.create({
    container: {
        paddingTop: 5,
        backgroundColor: '#FFF',
        borderRadius: 7,
        marginBottom: 7,
        height: heightWindow > 700 ? heightWindow * 0.35 : heightWindow * 0.3,
        borderRadius: 10,
        borderBottomWidth: 1,
        borderColor: "#ADADAD",

    },
})