import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

export default props => {
    return (
        <View style={[styles.container, props.style]}>
            <Text style={styles.texto}>{props.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 5,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: "flex-end",
        marginBottom: 15
    },
    texto: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#000000'
    }
})