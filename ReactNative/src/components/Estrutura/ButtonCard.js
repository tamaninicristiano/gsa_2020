import React from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const { width } = Dimensions.get('window')

export default props => (
    <View style={[styles.container, props.style, { width: (width - `${props.divPadding}`) / `${props.div}` }]}>
        <View style={styles.containerIcon}>
            <Text style={[styles.infoCard, props.styleInfo]}>{props.infoCard}</Text>
            <Icon name={props.icone} style={[styles.icon, props.styleIcon]} />
        </View>
        <View style={styles.containerTitle}>
            <Text style={styles.title}>{props.title}</Text>
        </View>
    </View>
)

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        borderRadius: 7,
        marginBottom: 15,
        height: 100,
        borderRadius: 10,
        borderBottomWidth: 3,
        borderRightWidth: 3,
        borderColor: "#ADADAD",
        justifyContent: 'center'
    },
    containerIcon: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 20,
        marginLeft: 15
    },
    icon: {
        fontSize: 35,
        color: '#130A52'
    },
    containerTitle: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginLeft: 20
    },
    title: {
        marginTop: 7,
        fontSize: 15,
    },
    infoCard: {
        marginTop: 7,
    }
})