import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

export default class Footer extends Component {
    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                {this.props.children}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 58,
        backgroundColor: '#2c3348',
    },
})