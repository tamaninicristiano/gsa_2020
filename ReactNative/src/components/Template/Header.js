import React, { Component } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import logo from '../../assets/img/LogoHeader.png'

export default class Header extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={logo} style={styles.logo} />
                {this.props.children}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#2c3348',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10
    },
    logo: {
        height: 35,
        width: 185,
        padding: 5
    },
    ButtonMenu: {
        width: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#2c3348"
    }
})