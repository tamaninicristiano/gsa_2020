import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet, ImageBackground, TouchableOpacity, Image, Dimensions, Alert } from 'react-native'
import { DrawerItems } from 'react-navigation-drawer'
import ImagemBack from '../../assets/img/Backgroud4.png'

const Height = Dimensions.get('window').height

import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/FontAwesome'

import logo from '../../assets/img/LogoHeader.png'

export default class Sidebar extends Component {
    state = {
        ip: '',
        porta: '',
        idImage: null,
        alturaFoto: 100,
        larguraFoto: 100,
        borderRadi: 50
    }

    componentDidMount = async () => {
        await this.conexaoDB()
        await this.dadosUsuario()
        await this.ajusteLogo()
    }

    ajusteLogo = async () => {
        if (Height > 815) {
            await this.setState({
                alturaFoto: 250,
                larguraFoto: 250,
                borderRadi: 125
            })
        } else if (Height > 750) {
            await this.setState({
                alturaFoto: 170,
                larguraFoto: 170,
                borderRadi: 85
            })
        } else {
            await this.setState({
                alturaFoto: 100,
                larguraFoto: 100,
                borderRadi: 50
            })
        }
    }

    conexaoDB = async () => {
        try {
            const servidorData = await AsyncStorage.getItem('dadosServidor')
            if (servidorData === null) {
                this.setState({
                    ip: '',
                    porta: ''
                })
            } else {
                let dados = JSON.parse(servidorData)
                this.setState({
                    ip: dados.ip,
                    porta: dados.porta
                })
            }
        } catch (e) {
            // UserData está invalido
        }
    }

    dadosUsuario = async () => {
        try {
            const usuarioCorrente = await AsyncStorage.getItem('usuarioCorrente')

            let dadosUs = JSON.parse(usuarioCorrente)
            await this.setState({
                idImage: dadosUs[0].ID
            })
        } catch (e) {
            // UserData está invalido
        }
    }

    logout = async () => {
        await delete axios.defaults.headers.common['Authorization']
        await AsyncStorage.removeItem('userData')
        await AsyncStorage.removeItem('usuarioCorrente')
        await this.props.navigation.navigate('AuthOrApp')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header}>
                    <ImageBackground source={ImagemBack} style={styles.ImagemBack}>
                        <View style={styles.containerProfile}>
                            <Image source={{ uri: `${this.state.ip}:${this.state.porta}/imagePerfis/${this.state.idImage}.png` }}
                                style={[styles.profile, { width: this.state.larguraFoto, height: this.state.alturaFoto, borderRadius: this.state.borderRadi }]} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingRight: 20 }}>
                            <View styles={styles.userInfo}>
                                <Text style={styles.TextNome}>{this.props.navigation.getParam('nome')}</Text>
                                <Text style={styles.TextEmail}>E-mail: {this.props.navigation.getParam('email')}</Text>
                                <Text style={styles.TextCNPJCPF}>CNPJ/CPF: {this.props.navigation.getParam('cnpjcpf')}</Text>
                            </View>
                            <TouchableOpacity onPress={this.logout}>
                                <View style={styles.logoutIcon}>
                                    <Icon name='sign-out' size={30} color='#800' />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
                <View style={styles.containerDrawerItems}>
                    <ScrollView>
                        <DrawerItems {...this.props} />
                    </ScrollView>
                </View>
                <View style={styles.Footer}>
                    <Image source={logo} style={styles.logo} />
                    <Text style={{ color: '#FFF', margin: 5, fontSize: 7 }}>Versão 1.2</Text>
                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerProfile: {
        alignItems: 'center'
    },
    profile: {
        minHeight: 100,
        minWidth: 100,
        borderRadius: Height > 975 ? 125 : 50,
        borderWidth: 3,
        borderColor: '#AAA'
    },
    Header: {
        flex: 4.5,
        borderBottomWidth: 1
    },
    ImagemBack: {
        flex: 1,
        paddingLeft: 16,
        width: undefined,
        paddingTop: 20
    },
    TextNome: {
        color: '#000000',
        fontSize: 20,
        fontWeight: "800",
        marginVertical: 8
    },
    TextEmail: {
        color: '#000000',
        fontSize: 12,
        marginLeft: 10
    },
    TextCNPJCPF: {
        color: '#000000',
        fontSize: 10,
        marginLeft: 10
    },
    logoutIcon: {
        marginLeft: 10,
        marginTop: 60
    },
    containerDrawerItems: {
        flex: 7
    },
    Footer: {
        height: 'auto',
        backgroundColor: '#2c3348',
        alignItems: 'center',
        justifyContent: 'flex-end',

    },
    logo: {
        height: 35,
        width: 185,
        padding: 5,
        marginTop: 5,
    }
})