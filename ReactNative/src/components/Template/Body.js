import React, { Component } from 'react'
import { StyleSheet, View, ImageBackground } from 'react-native'

export default class Footer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={this.props.img} style={styles.ImagemBack}>
                    {this.props.children}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%'
    },
    ImagemBack: {
        flex: 1,
        width: '100%',
    },
})