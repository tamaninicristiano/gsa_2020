import { Alert, Platform } from 'react-native'

const server = Platform.OS === 'ios' ?
    `192.168.0.14:3000` : `192.168.0.14:3000`

function showError(err) {
    Alert.alert('Ocorreu um Problema!', `Mensagem: ${err}`)
}

export { server, showError }