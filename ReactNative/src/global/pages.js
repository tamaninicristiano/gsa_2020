
//Financeiro
import ModFinanceiro from '../pages/Financeiro/ModuloFinanceiro'

//Suprimentos
import ModSuprimentos from '../pages/Suprimentos/ModuloSuprimentos'

//Faturamento
import ModFaturamento from '../pages/Faturamento/ModuloFaturamento'

//CadastrosDiversos
import ModCadastrosDiversos from '../pages/CadastrosDiversos/ModuloCadastrosDiversos'

export default {
    ModFinanceiro,
    ModFaturamento,
    ModSuprimentos,
    ModCadastrosDiversos
}