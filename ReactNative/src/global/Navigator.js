import React from 'react'
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createStackNavigator } from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/FontAwesome'
import Icon5 from 'react-native-vector-icons/FontAwesome5'

import Sidebar from '../components/Template/Sidebar'
import { Dimensions } from 'react-native'

//Filtros
import FiltroProduto from '../pages/Global/Filtros/Produtos'

//Telas
//Global
import Menu from '../pages/Global/Menu'
import Auth from '../pages/Global/Auth'
import AuthOrApp from '../pages/Global/AuthOrApp'
import ConfiguracoesConexao from '../pages/Global/ConfiguracoesConexao'
import Pessoa from '../pages/Global/Pessoa'
import AddFoto from '../components/Estrutura/AddFoto'

//Financeiro
import ModFinanceiro from '../pages/Financeiro/ModuloFinanceiro'

//Suprimentos
import ModSuprimentos from '../pages/Suprimentos/ModuloSuprimentos'
import ListaReservasProduto from '../pages/Suprimentos/pages/Listas/ListaReservasProdutos'
import ListaProdutos from '../pages/Suprimentos/pages/Listas/ListaProdutos'
import ProdutoDetalhes from '../pages/Suprimentos/pages/Forms/ProdutoDetalhes'
import ListaSolicitacaoCompraItens from '../pages/Suprimentos/pages/Listas/ListaSolicitacaoCompraItens'
import SolicitacaoCompraItem from '../pages/Suprimentos/pages/Forms/SolicitacaoCompraItem'
import ListaInventarioItens from '../pages/Suprimentos/pages/Listas/ListaInventarioItens'
import ListaInventarios from '../pages/Suprimentos/pages/Listas/ListaInventarios'

//Faturamento
import DashboardFaturamento from '../pages/Faturamento/pages/DashboardFaturamento'
import ModFaturamento from '../pages/Faturamento/ModuloFaturamento'
import Expedicoes from '../pages/Faturamento/pages/Expedicoes'
import ListaAtivosInativos from '../pages/Faturamento/pages/Listas/ListaAtivosInativos'
import ListaCotas from '../pages/Faturamento/pages/Listas/ListaCotas'
import ListaOrdensServicoManutencao from '../pages/Faturamento/pages/Listas/ListaOrdensServicoManutencao'
import ListaOrdensServicoVeiculos from '../pages/Faturamento/pages/Listas/ListaOrdensServicoVeiculos'
import OrdemServicoManutencao from '../pages/Faturamento/pages/Forms/OrdemServicoManutencao'
import OrdemServicoVeiculos from '../pages/Faturamento/pages/Forms/OrdemServicoVeiculos'
import AddProdutoServico from '../pages/Faturamento/pages/Forms/AddProdutoServico'
import AddProdutoServicoVeiculo from '../pages/Faturamento/pages/Forms/AddProdutoServicoVeiculo'

//CadastrosDiversos
import ModCadastrosDiversos from '../pages/CadastrosDiversos/ModuloCadastrosDiversos'
import ListaPessoas from '../pages/CadastrosDiversos/pages/ListaPessoas'
import FrmPessoa from '../pages/CadastrosDiversos/pages/FrmPessoa'
import ListaAnotacoes from '../pages/CadastrosDiversos/pages/Listas/ListaAnotacoes'

//Produção
import ListaComponentesProduto from '../pages/Producao/pages/ListaComponentesProduto'

const widthWindow = Dimensions.get("window").width

const menuConfig = {
    initialRouteName: 'Menu',
    contentComponent: Sidebar,
    drawerPosition: 'left',
    drawerWidth: widthWindow > 500 ? widthWindow * 0.50 : widthWindow * 0.80,
    hideStatusBar: false,
    contentOptions: {
        activeBackgroundColor: "rgba(64, 61, 61,0.2)",
        activeTintColor: "#000000",
        itemsContainerStyle: {
            marginTop: 16,
            marginHorizontal: 8
        },
        itemStyle: {
            borderRadius: 4
        }
    }
}

const menuRoutes = {
    Menu: {
        name: "Menu",
        screen: props => <Menu {...props} />,
        navigationOptions: {
            title: "Menu",
            header: null,
            drawerIcon: () => <Icon name='home' size={20} color="#000000" />
        }
    },
    ModCadastrosDiversos: {
        name: "Cadastros Diversos",
        screen: ModCadastrosDiversos,
        navigationOptions: {
            title: "Cadastros Diversos",
            header: null,
            drawerIcon: () => <Icon name='book' size={20} color="#000000" />
        }
    },
    ModFinanceiro: {
        name: "Financeiro",
        screen: ModFinanceiro,
        navigationOptions: {
            title: "Financeiro",
            header: null,
            drawerIcon: () => <Icon name='bank' size={20} color="#000000" />
        }
    },
    ModSuprimentos: {
        name: 'Suprimentos',
        screen: ModSuprimentos,
        navigationOptions: {
            title: "Suprimentos",
            header: null,
            drawerIcon: () => <Icon5 name='boxes' size={20} color="#000000" />
        }
    },
    ModFaturamento: {
        name: 'Faturamentoento',
        screen: ModFaturamento,
        navigationOptions: {
            title: "Faturamento",
            header: null,
            drawerIcon: () => <Icon5 name='flag-checkered' size={20} color="#000000" />,
        }
    },
    ConfiguracoesConexao: {
        name: 'ConfiguracoesConexao',
        screen: ConfiguracoesConexao,
        navigationOptions: {
            title: "Configurações",
            header: null,
            drawerIcon: () => <Icon name='gears' size={20} color="#000000" />,
        }
    },
    AddFoto: {
        name: 'AddFoto',
        screen: AddFoto,
        navigationOptions: {
            title: "AddFoto",
            header: null,
            drawerIcon: () => <Icon name='camera' size={20} color="#000000" />,
        }
    },
}

const MenuNavigator = createDrawerNavigator(menuRoutes, menuConfig)

const mainRoutes = {
    Auth: {
        name: 'Auth',
        screen: Auth,
        navigationOptions: {
            headerShown: false,
        }
    },
    AuthOrApp: {
        name: 'AuthOrApp',
        screen: AuthOrApp,
        navigationOptions: {
            headerShown: false,
        }
    },
    Menu: {
        name: 'Menu',
        screen: MenuNavigator,
        navigationOptions: {
            headerShown: false,
        }
    },
    Expedicoes: {
        name: 'Expedicoes',
        screen: Expedicoes,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaProdutos: {
        name: 'ListaProdutos',
        screen: ListaProdutos,
        navigationOptions: {
            headerShown: false,
        }
    },
    ConfiguracoesConexao: {
        name: 'ConfiguracoesConexao',
        screen: ConfiguracoesConexao,
        navigationOptions: {
            headerShown: false,
        }
    },
    ProdutoDetalhes: {
        name: 'ProdutoDetalhes',
        screen: ProdutoDetalhes,
        navigationOptions: {
            title: "Produto Detalhes",
            headerShown: false,
        }
    },
    Pessoa: {
        name: 'Pessoa',
        screen: Pessoa,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaReservasProduto: {
        name: 'ListaReservasProduto',
        screen: ListaReservasProduto,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaComponentesProduto: {
        name: 'ListaComponentesProduto',
        screen: ListaComponentesProduto,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaPessoas: {
        name: 'ListaPessoas',
        screen: ListaPessoas,
        navigationOptions: {
            headerShown: false,
        }
    },
    FrmPessoa: {
        name: 'FrmPessoa',
        screen: FrmPessoa,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaSolicitacaoCompraItens: {
        name: 'ListaSolicitacaoCompraItens',
        screen: ListaSolicitacaoCompraItens,
        navigationOptions: {
            headerShown: false,
        }
    },
    SolicitacaoCompraItem: {
        name: 'SolicitacaoCompraItem',
        screen: SolicitacaoCompraItem,
        navigationOptions: {
            headerShown: false,
        }
    },
    FiltroProduto: {
        name: 'FiltroProduto',
        screen: FiltroProduto,
        navigationOptions: {
            headerShown: false,
        }
    },
    DashboardFaturamento: {
        name: 'DashboardFaturamento',
        screen: DashboardFaturamento,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaAtivosInativos: {
        name: 'ListaAtivosInativos',
        screen: ListaAtivosInativos,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaAnotacoes: {
        name: 'ListaAnotacoes',
        screen: ListaAnotacoes,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaCotas: {
        name: 'ListaCotas',
        screen: ListaCotas,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaOrdensServicoManutencao: {
        name: 'ListaOrdensServicoManutencao',
        screen: ListaOrdensServicoManutencao,
        navigationOptions: {
            headerShown: false,
        }
    },
    OrdemServicoManutencao: {
        name: 'OrdemServicoManutencao',
        screen: OrdemServicoManutencao,
        navigationOptions: {
            headerShown: false,
        }
    },
    AddProdutoServico: {
        name: 'AddProdutoServico',
        screen: AddProdutoServico,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaOrdensServicoVeiculos: {
        name: 'ListaOrdensServicoVeiculos',
        screen: ListaOrdensServicoVeiculos,
        navigationOptions: {
            headerShown: false,
        }
    },
    OrdemServicoVeiculos: {
        name: 'OrdemServicoVeiculos',
        screen: OrdemServicoVeiculos,
        navigationOptions: {
            headerShown: false,
        }
    },
    AddProdutoServicoVeiculo: {
        name: 'AddProdutoServicoVeiculo',
        screen: AddProdutoServicoVeiculo,
        navigationOptions: {
            headerShown: false,
        }
    },
    AddFoto: {
        name: 'AddFoto',
        screen: AddFoto,
        navigationOptions: {
            headerShown: true,
        }
    },
    ListaInventarioItens: {
        name: 'ListaInventarioItens',
        screen: ListaInventarioItens,
        navigationOptions: {
            headerShown: false,
        }
    },
    ListaInventarios: {
        name: 'ListaInventarios',
        screen: ListaInventarios,
        navigationOptions: {
            headerShown: false,
        }
    },
}

const mainNavigator = createStackNavigator(mainRoutes, {
    initialRouteName: 'AuthOrApp',
});

export default createAppContainer(mainNavigator);